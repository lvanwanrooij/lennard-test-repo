library(tidyverse)
library(magrittr)
library(lubridate)

patientdica = readRDS("H:/My Documents/lennard-test-repo/input/dcra_2019_export_patient_final.rds");
patientiknl = readRDS("H:/My Documents/lennard-test-repo/input/dcra_2019_iknl_export_patient_final.rds");
tumordica = readRDS("H:/My Documents/lennard-test-repo/input/dcra_2019_export_tumor_final.rds");
tumoriknl = readRDS("H:/My Documents/lennard-test-repo/input/dcra_2019_iknl_export_tumor_final.rds");
comorbiditeitendica = readRDS("H:/My Documents/lennard-test-repo/input/dcra_2019_export_comorbiditeiten_final.rds");
comorbiditeiteniknl = readRDS("H:/My Documents/lennard-test-repo/input/dcra_2019_iknl_export_comorbiditeiten_final.rds");

dflist <- list(patientdica = patientdica,
               patientiknl = patientiknl,
               tumordica = tumordica,
               tumoriknl = tumoriknl,
               comorbiditeitendica = comorbiditeitendica,
               comorbiditeiteniknl = comorbiditeiteniknl)

list2env(lapply(dflist, function(x) {
  mutate_if(x, is.POSIXct, function(y){
    if_else(year(y) > year(today()) - 120 & year(y) <= year(today()), y, as.POSIXct(NA))
  })
}), envir = globalenv())

patientdica <- patientdica %>%
  distinct(uri, .keep_all = TRUE) %>%
  mutate(dataset_patient = "DICA")

patientiknl <- patientiknl %>%
  distinct(uri, .keep_all = TRUE) %>%
  mutate(dataset_patient = "IKNL")

tumordica <- tumordica %>%
  arrange(patient_uri, datpa1, desc(is_valid), desc(klaar), locprim, locprim2) %>%
  distinct(patient_uri, .keep_all = TRUE) %>%
  mutate(dataset_tumor = "DICA")

tumoriknl <- tumoriknl %>%
  arrange(patient_uri, datpa1, desc(is_valid), desc(klaar), locprim, locprim2) %>%
  distinct(patient_uri, .keep_all = TRUE) %>%
  mutate(dataset_tumor = "IKNL")

#cleanen comorbiditeiten dataset en berekening charlson score
comorbiditeitendica <- comorbiditeitendica %>% 
  arrange(patient_uri, uri) %>% 
  distinct(uri, .keep_all = T)

comorbiditeiteniknl <- comorbiditeiteniknl %>% 
  arrange(patient_uri, uri) %>% 
  distinct(uri, .keep_all = T)

patient <- bind_rows(patientdica, patientiknl) %>% 
  rename(patient_uri = uri)
tumor <- bind_rows(tumordica, tumoriknl)%>% 
  rename(tumor_uri = uri)
comorbiditeiten <- bind_rows(comorbiditeitendica, comorbiditeiteniknl)%>% 
  rename(comorbiditeiten_uri = uri)

df <- patient %>% 
  left_join(tumor, by = c("patient_uri" = "patient_uri")) 

df <- df %>% 
  left_join(comorbiditeiten, by = c("patient_uri" = "patient_uri"))

# write.table(df, "H:/My Documents/Test/df.txt", sep = "\t")
