library(tidyverse)
library(lubridate)
library(shiny)
library(DT)
library(rhandsontable)

dfl <- readRDS("../../input/covid19df.rds")
df2 <- readRDS("../../input/forcastsbyweek.rds")

perc = data.frame(Week = 1:53,
                  DSAA = 100,
                  DCRA = 100,
                  DHBA = 100,
                  DLCAS = 100,
                  DUCA = 100,
                  DPCA = 100)

ui <- fluidPage(
  
  titlePanel("DICA non COVID-19 capaciteitsplanner"),
  
  splitLayout(
    
    
    checkboxGroupInput("reg",
                       "Kies reg(s)",
                       unique(dfl$Registratie)
    ),
    
    checkboxGroupInput("typeok",
                       "Kies type operatie(s)",
                       unique(dfl$Type_Operatie)
    ),
    
    checkboxGroupInput("roaz",
                       "Kies regio('s)",
                       unique(dfl$Regio),
                       unique(dfl$Regio)
    ),
    
    rHandsontableOutput("editable_percentagesandwaitinglist"),
    
    plotOutput("plot1"),
    
    dataTableOutput("Resultaat1"),
    
    plotOutput("plot2"),
    
    dataTableOutput("Resultaat2"),
    
    cellWidths = c("5%", "5%", "5%", "15%", "20%", "10%", "20%", "10%")
  )
  
  
)

server <- function(input, output, session) {
  dfl <- readRDS("../../input/covid19df.rds")
  df2 <- readRDS("../../input/forcastsbyweek.rds")
  wl <- readRDS("../../input/wachtlijst.rds")
  
  observe({
    gekozenregistraties <- input$reg
    keuzes_typeok <- dfl %>% filter(Registratie %in% input$reg) %>% distinct(Type_Operatie) %>% pull
    
    updateCheckboxGroupInput(session,
                             "typeok",
                             "Kies type operatie(s)",
                             choices = keuzes_typeok)
    
  })
  
  perc = data.frame(Week = 1:53,
                    DSAA = 100,
                    DCRA = 100,
                    DHBA = 100,
                    DLCAS = 100,
                    DUCA = 100,
                    DPCA = 100)
  
  perc_and_wl <- bind_cols(perc, 
                           wl %>% 
                             bind_rows(data.frame(Registratie = "",
                                                  Type_Operatie = "",
                                                  Wachtlijst = NA,
                                                  stringsAsFactors = F))
  )
  
  datavalues <- reactiveValues(data=perc_and_wl)
  
  output$editable_percentagesandwaitinglist <- renderRHandsontable({
    rhandsontable(datavalues$data)
    
  })
  
  observeEvent(
    input$editable_percentagesandwaitinglist$changes$changes,
    {
      
      xi=input$editable_percentagesandwaitinglist$changes$changes[[1]][[1]]
      datavalues$data <- hot_to_r(input$editable_percentagesandwaitinglist)
      
    }
  )
  
  dflsel <- reactive({
    datavalues$data %>% 
      select(1:7) %>% 
      pivot_longer(-Week, "Registratie", values_to = "Capaciteit") %>% 
      right_join(filter(df2, Registratie %in% input$reg & Type_Operatie %in% input$typeok & Regio %in% input$roaz)) %>% 
      group_by(Registratie, Type_Operatie, Regio, Tijdseenheid) %>% 
      mutate(Capaciteit100 = cumsum(Operaties) * Percentage_Type * Percentage_Roaz * Aantal,
             CapaciteitCurrent = cumsum(Capaciteit / 100 * Operaties * Percentage_Type * Percentage_Roaz * Aantal),
             CapaciteitsVerlies = CapaciteitCurrent - Capaciteit100) %>% 
      group_by(Tijdseenheid, Week) %>% 
      summarise(Capaciteit100_agg = round(sum(Capaciteit100), 1),
                CapaciteitCurrent_agg = round(sum(CapaciteitCurrent), 1),
                CapaciteitsVerlies_agg = round(sum(CapaciteitsVerlies), 1)) %>% 
      ungroup()
    
  })
  
  dflsel2 <- reactive({
    datavalues$data %>% 
      select(8:10) %>% 
      right_join(
        filter(df2, Registratie %in% input$reg & Type_Operatie %in% input$typeok) %>% 
          distinct(Registratie, Type_Operatie, Tijdseenheid, Aantal)
      ) %>% 
      mutate(Wachtlijst_Bij_Aantal = Wachtlijst * Aantal) %>% 
      group_by(Registratie, Tijdseenheid) %>% 
      summarise(Wachtlijst_Bij_Aantal_agg = sum(Wachtlijst_Bij_Aantal)) %>% 
      ungroup()
  })
  
  
  output$plot1 <- renderPlot(ggplot(dflsel(),
                                    aes(x = Week, y = CapaciteitsVerlies_agg, fill = Tijdseenheid)) + geom_line())
  
  output$Resultaat1 <- renderDataTable({dflsel() %>% 
      group_by(Tijdseenheid) %>% 
      slice(n()) %>% 
      ungroup() %>% 
      select(Tijdseenheid, capvel = CapaciteitsVerlies_agg)
  })
  
  output$plot2 <- renderPlot(ggplot(dflsel2(),
                                    aes(x = Registratie, y = Wachtlijst_Bij_Aantal_agg, fill = Tijdseenheid)) + 
                               geom_bar(stat = "identity", position = "dodge"))
  
  output$Resultaat2 <- renderDataTable({dflsel2() %>% 
      group_by(Tijdseenheid) %>% 
      summarise(Wachtlijst_Bij_Aantal_agg_agg = sum(Wachtlijst_Bij_Aantal_agg))
    
  })
  
  session$onSessionEnded(function(){
    stopApp()
  })
}

runApp(list(ui = ui, server = server), launch.browser = T)
