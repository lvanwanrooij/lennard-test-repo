df <- readRDS("../../input/covid19df.rds")
forcasts <- readRDS("../../input/forcasts.rds")

y <- df %>% 
  filter(Registratie == "DCRA" & Type_Operatie == "Ileocoecaal resectie") %>% 
  distinct(Percentage_Type)

x = forcasts %>% 
  filter(Registratie == "DCRA") %>% 
  select(Operaties) %>% 
  pull %>% 
  sum


x * y * 3


forcasts %>% 
  filter(Registratie == "DCRA") %>% 
  mutate(Operaties_cumsum = cumsum(Capaciteit / 100 * Operaties)) %>% 
  select(Operaties_cumsum) %>% 
  tail(1)
 
df2 <- df %>% 
  left_join(forcasts)

df2 <- readRDS("../../input/forcastsbyweek.rds")

df2 %>% 
  # filter(Registratie %in% c("DCRA", "DSAA") & Type_Operatie %in% c("Hybride", "Ileocoecaal resectie")) %>% 
  left_join(
    pivot_longer(perc, -Week, "Registratie", values_to = "Capaciteit")
    ) %>% 
  group_by(Registratie) %>% 
  mutate(Operaties_cumsum = cumsum(Capaciteit / 100 * Operaties)) %>% 
  ungroup()



test <- df2 %>% 
  filter(Registratie %in% c("DCRA") & Type_Operatie %in% c("Ileocoecaal resectie")) %>%
  left_join(
    pivot_longer(perc, -Week, "Registratie", values_to = "Capaciteit")
  ) %>% 
  group_by(Registratie, Tijdseenheid) %>% 
  mutate(Capaciteit100 = cumsum(Operaties) * Percentage_Type * Aantal,
         CapaciteitCurrent = cumsum(Capaciteit / 100 * Operaties * Percentage_Type * Aantal),
         Capaciteitsverlies = CapaciteitCurrent - Capaciteit100) %>% 
  ungroup() %>% 
  distinct(Registratie, Week, Tijdseenheid, .keep_all = T)



test <- df2 %>% 
  filter(Registratie %in% "DCRA" & Type_Operatie %in% c("Ileocoecaal resectie", "Abdomino-perineale resectie (APR)") & Regio %in% c("Netwerk Acute Zorg Noordwest", "Acute Zorg Euregio")) %>% 
  left_join(
    pivot_longer(perc, -Week, "Registratie", values_to = "Capaciteit")
  ) %>% 
  group_by(Registratie, Type_Operatie, Regio, Tijdseenheid) %>% 
  mutate(Capaciteit100 = cumsum(Operaties) * Percentage_Type * Percentage_Roaz * Aantal,
         CapaciteitCurrent = cumsum(Capaciteit / 100 * Operaties * Percentage_Type * Percentage_Roaz * Aantal),
         CapaciteitsVerlies = CapaciteitCurrent - Capaciteit100) %>% 
  group_by(Tijdseenheid, Week) %>% 
  summarise(Capaciteit100_agg = sum(Capaciteit100),
            CapaciteitCurrent_agg = sum(CapaciteitCurrent),
            CapaciteitsVerlies_agg = sum(CapaciteitsVerlies)) %>% 
  ungroup()



test %>% 
  group_by(Tijdseenheid) %>% 
  slice(n()) %>% 
  ungroup() %>% 
  select(Tijdseenheid, Capaciteit100_agg)




datavalues2 <- reactiveValues(data=wl)

output$editable_percentages <- renderRHandsontable({
  rhandsontable(datavalues2$data)
  
})

observeEvent(
  input$editable_wachtlijst$changes$changes,
  {
    
    xi=input$editable_wachtlijst$changes$changes[[1]][[1]]
    datavalues2$data <- hot_to_r(input$editable_wachtlijst)
  }
  
)


perc_and_wl %>% 
  select(8:10) %>% 
  # right_join(filter(df2, Registratie %in% input$reg & Type_Operatie %in% input$typeok & Regio %in% input$roaz)) %>% 
  right_join(
    filter(df2, Registratie %in% "DCRA") %>% 
      distinct(Registratie, Type_Operatie, Tijdseenheid, Aantal)
    ) %>% 
  group_by(Registratie, Tijdseenheid) %>% 
  summarise(sum(Wachtlijst * Aantal)) %>% 
  ungroup()


check <- read.table(text = "8,96%	8,5047	0,8712	3
36,72%	8,3012	0,7182	3
10,68%	7,0272	0,4603	4
6,51%	9,2470	0,5466	5
0,47%	9,6866	0,8455	3
1,49%	2,1848	0,0000	2
0,15%	23,7500	0,1429	5
0,22%	25,6769	0,2909	5
0,15%	12,5882	0,0000	2
3,84%	7,3991	0,7650	4
0,16%	11,0426	1,0889	5
1,00%	11,1866	1,0495	0
15,11%	6,6547	0,6326	3
1,40%	13,8308	2,3188	3
10,65%	8,7500	0,6496	5
0,73%	4,4545	0,0649	3
1,60%	8,5088	1,0137	3
0,09%	12,1429	0,6522	2

")
check[] <- lapply(check, function(x) gsub(",", ".", x))  
check[] <- lapply(check, function(x) gsub("%", "", x))  
check[] <- lapply(check, as.numeric)
colSums(check[,-1])
