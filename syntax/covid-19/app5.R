library(tidyverse)
library(lubridate)
library(shiny)
library(DT)
library(rhandsontable)
library(extrafont)

dfl <- readRDS("../../input/covid19df.rds")

ui <- fluidPage(theme = "dica_style.css",
                
                div(
                  tags$img(src = "dica_logo.png", style = "position:absolute;top:20px;left:20px;width:100px;height:125;"),
                  tags$img(src = "allchiefs_logo.png", style = "position:absolute;top:20px;right:20px;width:100px;height:125;"),
                  style = "height:140px"),
                tags$h1("DICA non COVID-19 capaciteitsplanner"),
                
                #titlePanel(span(title = "non COVID-19 capaciteitsplanner", style = "font-size: 32px")),
                
                
                column(1,
                       
                       checkboxGroupInput("reg",
                                          "Kies registratie(s)",
                                          unique(dfl$Registratie)
                       ),
                       
                       checkboxGroupInput("roaz",
                                          "Kies regio('s)",
                                          unique(dfl$Regio),
                                          unique(dfl$Regio)
                       )
                ),
                
                column(1,
                       
                       checkboxGroupInput("typeok",
                                          "Kies operatie(s)",
                                          unique(dfl$Type_Operatie)
                       )
                ),
                
                
                column(6,
                       rHandsontableOutput("editable_percentagesandwaitinglist")
                ),
                
                column(4,
                       plotOutput("plot1"),
                       tableOutput("Resultaat1"),
                       plotOutput("plot2"),
                       tableOutput("Resultaat2")
                )
                
)




server <- function(input, output, session) {
  dfl <- readRDS("../../input/covid19df.rds")
  df2 <- readRDS("../../input/forcastsbyweek.rds")
  wl <- readRDS("../../input/wachtlijst.rds")
  
  observe({
    gekozenregistraties <- input$reg
    keuzes_typeok <- dfl %>% filter(Registratie %in% input$reg) %>% distinct(Type_Operatie) %>% pull
    
    updateCheckboxGroupInput(session,
                             "typeok",
                             "Kies operatie(s)",
                             choices = keuzes_typeok,
                             selected = keuzes_typeok[!keuzes_typeok %in% c("Overig DSAA", "Overig DCRA", "Onbekend DHBA", "Onbekend DLCA-S", "Overig DCLA-S", "Overig DUCA", "Overig DPCA")])
    
  })
  
  perc = data.frame(Week = 1:53,
                    DSAA = 100L,
                    DCRA = 100L,
                    DHBA = 100L,
                    DLCAS = 100L,
                    DUCA = 100L,
                    DPCA = 100L)
  
  perc_and_wl <- bind_cols(perc, 
                           wl)
  
  datavalues <- reactiveValues(data=perc_and_wl)
  
  output$editable_percentagesandwaitinglist <- renderRHandsontable({
    rhandsontable(datavalues$data %>% 
                    select(1, input$reg, "Registratie", "Type_Operatie", "Wachtlijst") %>% 
                    mutate(Registratie = case_when(Registratie %in% input$reg & Type_Operatie %in% input$typeok ~ Registratie,
                                                   TRUE ~ NA_character_),
                           Type_Operatie = case_when(Registratie %in% input$reg & Type_Operatie %in% input$typeok ~ Type_Operatie,
                                                     TRUE ~ NA_character_),
                           Wachtlijst = case_when(is.na(Registratie) | is.na(Type_Operatie) ~ "",
                                                  TRUE ~ as.character(Wachtlijst))),
                  rowHeaders = NULL 
    ) %>% 
      hot_col(c("Week", "Registratie", "Type_Operatie"), readOnly = T)
  })
  
  observeEvent(
    input$editable_percentagesandwaitinglist$changes$changes,
    {
      
      xi=input$editable_percentagesandwaitinglist$changes$changes[[1]][[1]]
      datavalues$data <- hot_to_r(input$editable_percentagesandwaitinglist)
      
    }
  )
  
  dflsel <- reactive({
    datavalues$data %>% 
      select("Week", input$reg) %>% 
      pivot_longer(-Week, "Registratie", values_to = "Capaciteit") %>% 
      right_join(filter(df2, Registratie %in% input$reg & Type_Operatie %in% input$typeok & Regio %in% input$roaz)) %>% 
      group_by(Registratie, Type_Operatie, Regio, Tijdseenheid) %>% 
      mutate(Capaciteit100 = cumsum(Operaties) * Percentage_Type * Percentage_Roaz * Aantal,
             CapaciteitCurrent = cumsum(Capaciteit / 100 * Operaties * Percentage_Type * Percentage_Roaz * Aantal),
             CapaciteitsVerlies = CapaciteitCurrent - Capaciteit100) %>% 
      group_by(Tijdseenheid, Week) %>% 
      summarise(Capaciteit100_agg = round(sum(Capaciteit100), 1),
                CapaciteitCurrent_agg = round(sum(CapaciteitCurrent), 1),
                CapaciteitsVerlies_agg = round(sum(CapaciteitsVerlies), 1)) %>% 
      ungroup()
    
  })
  
  dflsel2 <- reactive({
    datavalues$data %>% 
      select("Registratie", "Type_Operatie", "Wachtlijst") %>% 
      right_join(
        filter(df2, Registratie %in% input$reg & Type_Operatie %in% input$typeok) %>% 
          distinct(Registratie, Type_Operatie, Tijdseenheid, Aantal)
      ) %>% 
      mutate(Wachtlijst_Bij_Aantal = as.integer(Wachtlijst) * Aantal) %>% 
      group_by(Registratie, Tijdseenheid) %>% 
      summarise(wachtlijst_bij_aantal_agg = sum(Wachtlijst_Bij_Aantal)) %>% 
      ungroup()
  })
  
  
  output$plot1 <- renderPlot({ 
    
    
    req(input$reg)
    
    ggplot(dflsel(),
           aes(x = Week, y = CapaciteitsVerlies_agg, fill = Tijdseenheid)) + 
      geom_line(size = 1.14, color = c(rep("#4e87a0", 53), rep("#d4e3db", 53), rep("#d6cf40", 53))) +
      theme_bw() +
      theme(axis.line.x = element_line(colour = "#404545FF"),
            axis.title = element_text(colour = "#828585FF"),
            panel.grid.major.x = element_blank(),
            panel.grid.major.y = element_line(colour = "#D1D1CC"),
            panel.grid.minor.x = element_blank(),
            panel.grid.minor.y = element_line(colour = "#D1D1CC"),
            panel.border= element_blank(),
            plot.margin = unit(c(1,1,1,0), "cm"),
            text = element_text(family = "Corbel"),
            complete = F) +
      ylab("Capaciteitsverschil bij ingevulde parameters")
    
  })
  
  output$Resultaat1 <- renderTable({
    
    req(input$reg)
    
    dflsel() %>% 
      group_by(Tijdseenheid) %>% 
      slice(n()) %>% 
      ungroup() %>%
      select(Tijdseenheid, Capaciteit100_agg, CapaciteitsVerlies_agg) %>%
      mutate(Tijdseenheid = gsub("_", " ", Tijdseenheid),
             Percentageverschil = paste0(round(100 * CapaciteitsVerlies_agg / Capaciteit100_agg, 1), "%")) %>% 
      rename('Capaciteit bij 100%'= Capaciteit100_agg,
             'Capaciteits overschot/tekort' = CapaciteitsVerlies_agg)
  })
  
  output$plot2 <- renderPlot({
    
    req(input$reg)
    
    ggplot(dflsel2(),
           aes(x = Registratie, y = wachtlijst_bij_aantal_agg, fill = Tijdseenheid)) + 
      geom_bar(stat = "identity", position = "dodge") + 
      scale_fill_manual(values = c("#4e87a0", "#d4e3db", "#d6cf40")) + 
      theme_bw() +
      theme(axis.line.x = element_line(colour = "#404545FF"),
            axis.title = element_text(colour = "#828585FF"),
            panel.grid.major.x = element_blank(),
            panel.grid.major.y = element_line(colour = "#D1D1CC"),
            panel.grid.minor.x = element_blank(),
            panel.grid.minor.y = element_line(colour = "#D1D1CC"),
            panel.border= element_blank(),
            plot.margin = unit(c(1,1,1,0), "cm"),
            text = element_text(family = "Corbel"),
            complete = F) +
      ylab("Wachtlijst bij ingevulde parameters")
    
    
  })
  
  output$Resultaat2 <- renderTable({
    
    req(input$reg)
    
    dflsel2() %>% 
      group_by(Tijdseenheid) %>% 
      summarise('Aantal op Wachtlijst' = round(sum(wachtlijst_bij_aantal_agg), 1)) %>%
      mutate(Tijdseenheid = gsub("_", " ", Tijdseenheid))
  })
  
  session$onSessionEnded(function(){
    stopApp()
  })
}

shinyApp(ui = ui, server = server)
#runApp(list(ui = ui, server = server), launch.browser = T)
