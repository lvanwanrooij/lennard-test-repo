library(devtools)
# library(lubridate)
# library(car)
# library(tidyverse)
# library(zoo)
# library(scales)
# LAAD HET DICA-PACKAGE
load_all("S:/Scripting&Onderwijs/3.staging/packages/dicapackage")

patient <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_patient_final.rds")
intake <-readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_intake_final.rds")
laboratorium <-  readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_laboratorium_final.rds")
lichond <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_lichonderzoek_final.rds")
leefrisico  <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_leefrisico_final.rds")
behandelaar <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_behandelaar_final.rds")
medicatiereg <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_medicatieregistratie_final.rds")
medicatie <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_medicatie_final.rds")
oogonderzoek <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_oogonderzoek_final.rds")
voetonderzoek <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_voetonderzoek_final.rds")
aandoeningen <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_aandoening_final.rds")
aandoening <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_aandoeningen_final.rds")

intakesmetpturi <- intake %>% 
  select(patient_uri, uri) %>% 
  rename(intake_uri = uri)

patient2 <- patient %>% 
  rename(patient_uri = uri)

intake2 <- intake %>% 
  arrange(patient_uri, desc(intakedat)) %>% 
  distinct(patient_uri, .keep_all = T)


selectievanpatientenobvdatum <- intake %>% 
  rename(intake_uri = uri) %>% 
  left_join(laboratorium, by = "intake_uri") %>% 
  select(intake_uri, patient_uri, intakedat, labdat, hba1cmol, hba1cperc) %>% 
  mutate(geschikte_datum  = case_when(as.Date(intakedat) %in% seq.Date(as.Date("2018-03-16"), as.Date("2018-05-24"), "days") |
                                        as.Date(intakedat) %in% seq.Date(as.Date("2019-03-16"), as.Date("2019-05-24"), "days") ~ intakedat,
                                      between(coalesce(hba1cmol, hba1cperc * 10.93 - 23.5), 25, 150) & (
                                        as.Date(labdat) %in% seq.Date(as.Date("2018-03-16"), as.Date("2018-05-24"), "days") |
                                          as.Date(labdat) %in% seq.Date(as.Date("2019-03-16"), as.Date("2019-05-24"), "days")) ~ labdat,
                                      TRUE ~  as.POSIXct(NA))) %>% 
  drop_na(geschikte_datum) %>% 
  arrange(patient_uri, desc(as.Date(geschikte_datum))) %>% 
  distinct(patient_uri, .keep_all = T) %>% 
  select(patient_uri, intake_uri, geschikte_datum)

laboratorium2 <- laboratorium %>% 
  filter(as.Date(labdat) %in% seq.Date(as.Date("2018-03-16"), as.Date("2018-05-24"), "days") |
           as.Date(labdat) %in% seq.Date(as.Date("2019-03-16"), as.Date("2019-05-24"), "days")) %>% 
  left_join(intakesmetpturi, by = "intake_uri") %>% 
  relocate(patient_uri) %>% 
  arrange(patient_uri, desc(labdat)) %>% 
  group_by(patient_uri) %>% 
  mutate(across(.fns = ~na.locf(.x, na.rm = F))) %>%
  mutate(across(.fns = ~na.locf(.x, na.rm = F, fromLast = T))) %>% 
  ungroup() %>% 
  distinct(patient_uri, .keep_all = T)

lichond2 <- lichond %>% 
  filter(as.Date(lichdat) %in% seq.Date(as.Date("2018-03-16"), as.Date("2018-05-24"), "days") |
           as.Date(lichdat) %in% seq.Date(as.Date("2019-03-16"), as.Date("2019-05-24"), "days")) %>% 
  left_join(intakesmetpturi, by = "intake_uri") %>% 
  relocate(patient_uri) %>% 
  arrange(patient_uri, desc(lichdat)) %>% 
  group_by(patient_uri) %>% 
  mutate(across(.fns = ~na.locf(.x, na.rm = F))) %>%
  mutate(across(.fns = ~na.locf(.x, na.rm = F, fromLast = T))) %>% 
  ungroup() %>% 
  distinct(patient_uri, .keep_all = T)

leefrisico2 <- leefrisico %>% 
  filter(as.Date(leefdat) %in% seq.Date(as.Date("2018-03-16"), as.Date("2018-05-24"), "days") |
           as.Date(leefdat) %in% seq.Date(as.Date("2019-03-16"), as.Date("2019-05-24"), "days")) %>% 
  left_join(intakesmetpturi, by = "intake_uri") %>% 
  relocate(patient_uri) %>% 
  arrange(patient_uri, desc(leefdat)) %>% 
  group_by(patient_uri) %>% 
  mutate(across(.fns = ~na.locf(.x, na.rm = F))) %>%
  mutate(across(.fns = ~na.locf(.x, na.rm = F, fromLast = T))) %>% 
  ungroup() %>% 
  distinct(patient_uri, .keep_all = T)

behandelaar2 <- behandelaar %>% 
  filter(as.Date(behdat) %in% seq.Date(as.Date("2018-03-16"), as.Date("2018-05-24"), "days") |
           as.Date(behdat) %in% seq.Date(as.Date("2019-03-16"), as.Date("2019-05-24"), "days")) %>% 
  left_join(intakesmetpturi, by = "intake_uri") %>% 
  relocate(patient_uri) %>% 
  arrange(patient_uri, desc(behdat)) %>% 
  group_by(patient_uri) %>% 
  mutate(across(.fns = ~na.locf(.x, na.rm = F))) %>%
  mutate(across(.fns = ~na.locf(.x, na.rm = F, fromLast = T))) %>% 
  ungroup() %>% 
  distinct(patient_uri, .keep_all = T)

oogonderzoek2 <- oogonderzoek %>% 
  filter(as.Date(oogdat) %in% seq.Date(as.Date("2018-03-16"), as.Date("2018-05-24"), "days") |
           as.Date(oogdat) %in% seq.Date(as.Date("2019-03-16"), as.Date("2019-05-24"), "days")) %>% 
  left_join(intakesmetpturi, by = "intake_uri") %>% 
  relocate(patient_uri) %>% 
  arrange(patient_uri, desc(oogdat)) %>% 
  group_by(patient_uri) %>% 
  mutate(across(.fns = ~na.locf(.x, na.rm = F))) %>%
  mutate(across(.fns = ~na.locf(.x, na.rm = F, fromLast = T))) %>% 
  ungroup() %>% 
  distinct(patient_uri, .keep_all = T)

voetonderzoek2 <- voetonderzoek %>% 
  filter(as.Date(voetdat) %in% seq.Date(as.Date("2018-03-16"), as.Date("2018-05-24"), "days") |
           as.Date(voetdat) %in% seq.Date(as.Date("2019-03-16"), as.Date("2019-05-24"), "days")) %>% 
  left_join(intakesmetpturi, by = "intake_uri") %>% 
  relocate(patient_uri) %>% 
  arrange(patient_uri, desc(voetdat)) %>% 
  group_by(patient_uri) %>% 
  mutate(across(.fns = ~na.locf(.x, na.rm = F))) %>%
  mutate(across(.fns = ~na.locf(.x, na.rm = F, fromLast = T))) %>% 
  ungroup() %>% 
  distinct(patient_uri, .keep_all = T)

df <- selectievanpatientenobvdatum %>% 
  left_join(patient2, by = c("patient_uri")) %>%
  left_join(intake2, by = c("patient_uri")) %>%
  left_join(laboratorium2, by = c("patient_uri")) %>% 
  left_join(lichond2, by = c("patient_uri")) %>% 
  left_join(leefrisico2, by = c("patient_uri")) %>% 
  left_join(behandelaar2, by = c("patient_uri")) %>% 
  left_join(oogonderzoek2, by = c("patient_uri")) %>% 
  left_join(voetonderzoek2, by = c("patient_uri")) %>% 
  select(-contains("intake_uri"))


medicatie2 <- medicatiereg %>%
  rename(medicatieregistratie_uri = uri) %>%
  left_join(intakesmetpturi, by = "intake_uri") %>%
  left_join(medicatie, by = "medicatieregistratie_uri") %>%
  relocate(contains("uri")) %>%
  select(-c(medicatieregistratie_uri, intake_uri, uri, is_valid.x, is_valid.y, organisation_uri.x, organisation_uri.y)) %>%
  arrange(patient_uri, meddat) %>%
  group_by(patient_uri) %>%
  mutate(rownr = row_number()) %>%
  ungroup %>%
  pivot_wider(patient_uri, values_from = -c(patient_uri, rownr), names_from = rownr)

df <- df %>%
  left_join(medicatie2, by = "patient_uri")

