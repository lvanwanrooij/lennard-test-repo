files <- list.files("H:/My Documents/dica-indicator-syntax/syntax/kpi-2020", pattern = "\\.R$", full.names = T, recursive = T)

files <- files[nchar(files) > 73]
files <- files[-grep("clinical-update-status|dgeadrce", files)]


error_when_sourcing <- c()
for(i in 1 : length(files)){
  #setwd(sub("/[^/]+$", "", files[i]))
  error <- try(source(files[i], chdir = TRUE))[1]
  if (error == "NULL"){
    error_when_sourcing[i] <- paste(gsub("H:/My Documents/dica-indicator-syntax/syntax/", "", files)[i], "Script_ran_successfully", sep = "@___@")
  } else {
    error_when_sourcing[i] <- paste(gsub("H:/My Documents/dica-indicator-syntax/syntax/", "", files)[i], error, sep = "@___@")
  }
}

sourcing_errorlog <- data.frame(script = sapply(sapply(error_when_sourcing, strsplit, "@___@"), "[", 1),
                                error = sapply(sapply(error_when_sourcing, strsplit, "@___@"), "[", 2),
                                row.names = NULL, stringsAsFactors = F)
sapply(sapply(error_when_sourcing, strsplit, "@___@"), "[", 1)
nchar("H:/My Documents/dica-indicator-syntax/syntax/kpi-2020/dgeadrce/dgeadrce.R")

error_when_sourcing
files


hoc <- readRDS("S:/Afdelingen/Data Science Team/hoc.rds")
hoc %>% 
  filter(error != "Script_ran_successfully") %>% 
  filter(!grepl("proms|prems|casemix|dsaa-failure-to-rescue", script)) %>% 
  filter(grepl("dato", script))

saveRDS(sourcing_errorlog, "H:/My Documents/hoc.rds")
hoc <- readRDS("H:/My Documents/hoc.rds")
