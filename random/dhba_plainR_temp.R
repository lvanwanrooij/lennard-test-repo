setwd("H:/My Documents/dica-indicator-syntax/syntax/dhba/")

patient = readRDS("../../input/dhba_2019_patient.current.rds")
verrichting = readRDS("../../input/dhba_2019_verrichting.current.rds")
followup = readRDS("../../input/dhba_2019_followup.current.rds")

library(dplyr)

patient %<>%
  rename(patient_uri = uri)
verrichting  %<>%
  rename(verrichting_uri = uri)
followup  %<>%
  rename(followup_uri = uri)

## Alle POSIXct velden schonen ##
dflist <- list(patient = patient,
               verrichting = verrichting,
               followup = followup)


list2env(lapply(dflist, function(x) {
  mutate_if(x, is.POSIXct, function(y){
    if_else(year(y) > year(today()) - 120 & year(y) <= year(today()), y, as.POSIXct(NA))
  })
}), envir = globalenv())

verrichting$xdatok1 <- if_else(verrichting$datok1 < 2015 | verrichting$datok1 > today(), 0, 1)
verrichting$xdatbez1 <- if_else(verrichting$datbez1 < 2015 | verrichting$datbez1 > today(), 0, 1)

# verrichting schonen: Klopt dit nu?
verrichting %<>%
  arrange(patient_uri, datok1, xdatbez1, status, reggroep, procok) %>%
  distinct(patient_uri, datok1, xdatbez1, .keep_all = TRUE) %>%
  filter(!is.na(xdatok1) | !is.na(reggroep)) %>%
  mutate(recidief_ver = if_else(datok1 == verr_datverr, 1, 0)) #aanmaken recidief_ver: is datok1 hetzelfde als verr_datverr

followup %<>%
  filter(year(fu_survdat) <= year(today())) # & !is.na(fu_survdat) voor nu eruit. Moet erin als gekoppeld aan patient. Dan: (!is.na(fu_survdat) & !is.na(datovl))

### Stap 7: Mergen
df <- verrichting %>% 
  left_join(patient, by  = "patient_uri")

# Hoe koppelen we followup aan df? Zijn er verrichtingen of fups die niet interessant zijn?
# lege fu_survdat of fu_survdat in de toekomst


# include_1r("xdatonts",0,
#            year(df$datont) >= 2011 & year(df$datont) <= year(today()), 1)
df %<>% 
  left_join(followup, by = "patient_uri") %>%
  mutate(recidief_fu = if_else(datok1 == fu_datverr, 1, 0)) %>%
  mutate(reci = if_else(datok1 == verr_datverr | datok1 == fu_datverr, 1, 0)) %>%
  mutate(fu_per = difftime(date(fu_survdat), date(datok1), units = "days")) %>%
  mutate(fu_1 = if_else(difftime(date(fu_survdat), date(datok1), units = "days") %in% c(365:1050), 1, 0)) %>%
  mutate(fu_2 = if_else(difftime(date(fu_dfs), date(datok1), units = "days") %in% c(365:1050), 1, 0))



#obv regels in signaleringslijst

# Fusietabel toepassen
# setwd("H:/My Documents/dica-indicator-syntax/syntax/dhba/")
# df$id_fusie = fusie_hospital_id("DHBA", df$verrichting_id)
df$id_fusie = df$verrichting_id

# test <- df %>% filter(!is.na(fu_datrec) | !is.na(fu_datverr) | !is.na(fu_dfs) | !is.na(fu_survdat)) %>% select(patient_uri, verrichting_uri, followup_uri, recidief_ver, verr_datverr, recidief_fu, reci, fu_datrec, fu_datverr, fu_survdat, datok1, fu_1, fu_2, fu_per) %>% View()

library(knitr)
df %>% filter(!is.na(fu_datrec) | !is.na(fu_datverr) | !is.na(fu_dfs) | !is.na(fu_survdat)) %>% 
  select(reci, fu_datrec, fu_datverr, fu_survdat, datok1, fu_1, fu_2, fu_per) %>% 
  kable()

df %>%
  filter(!is.na(fu_survdat) == TRUE) %>%
  tally(!is.na(fu_survdat), name = "Follow-up censordatum beschikbaar:") %>%
  kable()

### Totaal aantal ingevulde follow-up modules 2018

# filter voor 2018
df %>%
  filter(year(fu_survdat) == 2018) %>%
  tally(!is.na(fu_survdat), name = "Follow-up censordatum beschikbaar voor 2018:") %>%
  kable()


### Totaal aantal ingevulde follow-up modules 2019

#filter voor 2019
df %>%
  filter(year(fu_survdat) == 2019) %>%
  tally(!is.na(fu_survdat), name = "Follow-up censordatum beschikbaar voor 2019:") %>%
  kable()

df %>% 
  mutate(verrichtingbinnenjaar = datok1 >= "2017-09-01" & datok1 <  "2018-09-01",
         followupaanwezig = datok1 >= "2018-09-01") %>% 
  filter(followupaanwezig == 1) %>% 
  tally(followupaanwezig, name = "Aantal verrichtingen na 2018-09-01:") %>% 
  kable()


df %>% 
  mutate(verrichtingbinnenjaar = datok1 >= "2017-09-01" & datok1 <  "2018-09-01",
         followupaanwezig = datok1 >= "2018-09-01") %>% 
  summarize(`Percentage follow-up beschikbaar:` = round(100 * sum(followupaanwezig, na.rm = T) / sum(verrichtingbinnenjaar, na.rm = T), 1)) %>% 
  kable()

df %>%
  filter(!is.na(fu_survdat) == TRUE) %>%
  arrange(id_fusie) %>% 
  mutate(id_fusie = letters[cumsum(c(1, diff(id_fusie)) != 0)]) %>% 
  group_by(id_fusie) %>% 
  tally(!is.na(fu_survdat), name = "per fusie hoe vaak fu_survdat beschikbaar") %>%
  kable()

df %>%
  filter(!is.na(fu_survdat) == TRUE) %>%
  arrange(id_fusie) %>% 
  mutate(id_fusie = letters[cumsum(c(1, diff(id_fusie)) != 0)]) %>% 
  group_by(id_fusie) %>% 
  tally(!is.na(fu_survdat), name = "per fusie hoe vaak fu_survdat beschikbaar") %>% 
  ggplot(aes(x = id_fusie, y = `per fusie hoe vaak fu_survdat beschikbaar`)) + 
  geom_bar(stat = "identity") +
  xlab("ziekenhuis") +
  ylab("frequentie fu_survdat beschikbaar") +
  ggtitle("Ziekenhuizen en beschikbaarheid fu_survdat") +
  theme_bw() + 
  theme(plot.title = element_text(hjust = 0.5))

df_survfu <- df %>%
  filter(!is.na(fu_datrec) & !is.na(fu_datverr)) %>%
  mutate(intervalfu = date(fu_datrec) - date(fu_datverr)) 

### Gemiddelde verschil in tijd tussen verr-datrec - verr-datverr uit verrichting module. 
df %>%
  filter(!is.na(verr_datrec) & !is.na(verr_datverr)) %>%
  mutate(intervalfu = difftime(date(fu_datrec), date(fu_datverr), units = "days"),
         intervalverr = difftime(date(verr_datrec), date(verr_datverr), units = "days")) %>%
  mutate(interval = ifelse(is.na(intervalfu), intervalverr, intervalfu)) %>%
  filter(interval >= 0) %>% 
  summarise(min = quantile(interval, probs=0, na.rm = T),
            `25%` = quantile(interval, probs=0.25, na.rm = T),
            `50%` = quantile(interval, probs=0.5, na.rm = T),
            `75%` = quantile(interval, probs=0.75, na.rm = T),
            max = quantile(interval, probs=1, na.rm = T),
            n=n()) %>% 
  kable()


### nog een tabel: karakteristieken survivaltime wanneer datovl beschikbaar is

# 
df %>%
  mutate(survivaltime = difftime(date(datovl), date(datok1), units = "days")) %>%
  filter(!is.na(datovl) & survivaltime >= 0) %>% 
  summarise(min = quantile(survivaltime, probs=0, na.rm = T),
            `25%` = quantile(survivaltime, probs=0.25, na.rm = T),
            `50%` = quantile(survivaltime, probs=0.5, na.rm = T),
            `75%` = quantile(survivaltime, probs=0.75, na.rm = T),
            max = quantile(survivaltime, probs=1, na.rm = T),
            n=n()) %>% 
  kable()

### en nog een tabel: Aantal datok1 vóór vandaag 1 jaar geleden:

df %>%
  tally(datok1 <= today() - years(1), name = "Aantal datok1 vóór vandaag 1 jaar geleden:") %>% 
  kable()




df %>%
  arrange(patient_uri, desc(datok1), datovl) %>%
  distinct(patient_uri, .keep_all = T) %>%
  mutate(survivaltime = difftime(date(datovl), date(datok1), units = "days")) %>%
  filter(survivaltime >= 0) %>% 
  summarise(min = quantile(survivaltime, probs=0, na.rm = T),
            `25%` = quantile(survivaltime, probs=0.25, na.rm = T),
            `50%` = quantile(survivaltime, probs=0.5, na.rm = T),
            `75%` = quantile(survivaltime, probs=0.75, na.rm = T),
            max = quantile(survivaltime, probs=1, na.rm = T),
            n=n()) %>% 
  kable()


suppressPackageStartupMessages(library(survival))
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(survminer))


df2 <- df %>%
  drop_na(fu_survdat) %>%
  mutate(fuos = ifelse(is.na(datovl), date(fu_survdat) - date(datok1), date(datovl) - date(datok1)), #verschil # mag niet negatief zijn)
         dood = ifelse((!is.na(datovl) | status == 0) & fuos > 365, 1, 0),
         resectievg = ifelse(resectievg %in% c(0,1), resectievg, NA))
km2 <- survfit(Surv(df2$fuos,  df2$dood) ~ df2$resectievg)

ggsurvplot(data = df2,
           fit = km2)

