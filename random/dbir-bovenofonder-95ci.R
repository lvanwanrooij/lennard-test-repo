df2 <- df %>% 
  mutate(jaarrap = case_when(jaarrap %in% c(2015:2018) ~ "range_of_interest",
                             TRUE ~ "years outside of scope")) %>% 
  rename(provider = id_fusie,
         year = jaarrap,
         numerator = starts_with("teller"),
         denominator = starts_with("noemer")) %>% 
  group_by(provider, year) %>% 
  mutate(numerator = sum(numerator, na.rm = T),
            denominator = sum(denominator, na.rm = T),
            proportion = numerator / denominator) %>% 
  ungroup() %>% 
  drop_na(proportion) %>% 
  group_by(year) %>% 
  mutate(numerator_region = sum(numerator, na.rm = T),
            denominator_region = sum(denominator, na.rm = T),
            benchmark = numerator_region / denominator_region) %>% 
  ungroup() %>% 
  drop_na(benchmark) %>% 
  mutate(lower95ci_at_current_denominator = exp(-qnorm(0.975) / sqrt(denominator * benchmark)) * benchmark,
         upper95ci_at_current_denominator = exp(qnorm(0.975) / sqrt(denominator * benchmark)) * benchmark,
         score_classifier = case_when(proportion < lower95ci_at_current_denominator ~ "below 95% CI",
                                      proportion > upper95ci_at_current_denominator ~ "above 95% CI",
                                      TRUE ~ "Within 95% CI"))


table(df2$score_classifier, useNA = "always")   
