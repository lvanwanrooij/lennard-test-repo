#dicapack inladen
devtools::load_all("S:/Scripting&Onderwijs/3.staging/packages/dicapackage")

#datasets inlezen
# patient_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.patient.rds")
intake_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.intake.rds")
laboratorium_2019 =  read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.laboratorium.rds")
lichonderzoek_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.lichonderzoek.rds")
leefrisico_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.leefrisico.rds")
behandelaar_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.behandelaar.rds")
medicatiereg_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.medicatieregistratie.rds")
medicatie_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.medicatie.rds")
oogonderzoek_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.oogonderzoek.rds")
voetonderzoek_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.voetonderzoek.rds")
aandoeningen_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.aandoeningen.rds")
aandoening_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.aandoening.rds")
patient_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.patient.rds")
intake_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.intake.rds")
laboratorium_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.laboratorium.rds")
lichonderzoek_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.lichonderzoek.rds")
leefrisico_2020  = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.leefrisico.rds")
behandelaar_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.behandelaar.rds")
medicatiereg_2020 =  read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.medicatieregistratie.rds")
medicatie_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.medicatie.rds")
oogonderzoek_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.oogonderzoek.rds")
voetonderzoek_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.voetonderzoek.rds")
aandoeningen_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.aandoeningen.rds")
aandoening_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.aandoening.rds")



# # patient_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.patient.rds")
# intake_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.intake.rds")
# laboratorium_2019 =  read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.laboratorium.rds")
# lichonderzoek_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.lichonderzoek.rds")
# leefrisico_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.leefrisico.rds")
# behandelaar_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.behandelaar.rds")
# medicatiereg_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.medicatieregistratie.rds")
# medicatie_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.medicatie.rds")
# oogonderzoek_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.oogonderzoek.rds")
# voetonderzoek_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.voetonderzoek.rds")
# aandoeningen_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.aandoeningen.rds")
# aandoening_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2019.aandoening.rds")
# patient_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.patient.rds")
# intake_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.intake.rds")
# laboratorium_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.laboratorium.rds")
# lichonderzoek_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.lichonderzoek.rds")
# leefrisico_2021  = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.leefrisico.rds")
# behandelaar_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.behandelaar.rds")
# medicatiereg_2021 =  read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.medicatieregistratie.rds")
# medicatie_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.medicatie.rds")
# oogonderzoek_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.oogonderzoek.rds")
# voetonderzoek_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.voetonderzoek.rds")
# aandoeningen_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/20210602/dpard_2021.aandoeningen.rds")
# aandoening_2021 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data//dpard_2021.aandoening.rds")




#datasets binden
behandelaar <- bind_rows(behandelaar_2019, behandelaar_2020)
intake <- bind_rows(intake_2019, intake_2020)
laboratorium <- bind_rows(laboratorium_2019, laboratorium_2020)
leefrisico <- bind_rows(leefrisico_2019, leefrisico_2020)
lichonderzoek <- bind_rows(lichonderzoek_2019, lichonderzoek_2020)
oogonderzoek <- bind_rows(oogonderzoek_2019, oogonderzoek_2020)
voetonderzoek <- bind_rows(voetonderzoek_2019, voetonderzoek_2020)
medicatiereg <- bind_rows(medicatiereg_2019, medicatiereg_2020)
medicatie <- bind_rows(medicatie_2019, medicatie_2020)

#hulpdatasets aanmaken
intakesmetpturi <- patient_2020 %>% 
  right_join(intake, by = "patient_uri") %>% 
  select(patient_uri, intake_uri)

intakesmetdatvar <- patient_2020 %>% 
  right_join(intake, by = "patient_uri") %>% 
  mutate(samengevoegde_datvar = coalesce(polibezdat, intakedat), .after = patient_uri) %>% 
  select(patient_uri, intake_uri, samengevoegde_datvar)

#datasets cleanen
behandelaar_clean <- intakesmetpturi %>% 
  right_join(behandelaar, by = "intake_uri") %>% 
  select(-c(behandelaar_uri, intake_uri))

intake_clean <- intake %>% 
  mutate(samengevoegde_datvar = coalesce(polibezdat, intakedat), .after = patient_uri,
         intake_geweest = 1) %>% 
  select(-c(polibezdat, intakedat, intake_uri))

laboratorium_clean <- intakesmetdatvar %>% 
  right_join(laboratorium, by = "intake_uri") %>% 
  select(-c(laboratorium_uri, intake_uri)) %>% 
  mutate(hba1cmol = if_else(between(hba1cmol, 25, 150), hba1cmol, NA_integer_),
         hba1cperc = if_else(between(hba1cperc * 10.93 - 23.5, 25, 150), hba1cperc, NA_real_),
         labdat = coalesce(labdat, samengevoegde_datvar),
         hba1c_gecombineerd = coalesce(hba1cmol, hba1cperc * 10.93 - 23.5),
         laboratorium_geweest = 1) %>% 
  select(-samengevoegde_datvar)

leefrisico_clean <- intakesmetpturi %>% 
  right_join(leefrisico, by = "intake_uri") %>% 
  select(-c(leefrisico_uri, intake_uri)) %>% 
  mutate(leefrisico_geweest = 1)

lichonderzoek_clean <- intakesmetdatvar %>% 
  right_join(lichonderzoek, by = "intake_uri") %>% 
  select(-c(lichonderzoek_uri, intake_uri)) %>% 
  mutate(lichdat = coalesce(lichdat, samengevoegde_datvar),
         lichonderzoek_geweest = 1) %>% 
  select(-samengevoegde_datvar)

oogonderzoek_clean <- intakesmetpturi %>% 
  right_join(oogonderzoek, by = "intake_uri") %>% 
  select(-c(oogonderzoek_uri, intake_uri)) %>% 
  mutate(oogonderzoek_geweest = 1)

voetonderzoek_clean <- intakesmetpturi %>% 
  right_join(voetonderzoek, by = "intake_uri") %>% 
  select(-c(voetonderzoek_uri, intake_uri)) %>% 
  mutate(voetonderzoek_geweest = 1, .after = voetdat)

medicatiereg_clean <- medicatiereg

medicatie_clean <- medicatie

#datasets bewerken tot 1 rij per patient per jaar
behandelaar_ready <- behandelaar_clean %>% 
  mutate(jaar = year(behdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(behdat)) %>% 
  group_by(patient_uri, jaar) %>% 
  fill(-matches("behdat"), .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, .keep_all = T) %>% 
  select(-behdat)

intake_ready <- intake_clean %>% 
  mutate(jaar = year(samengevoegde_datvar)) %>% 
  arrange(patient_uri, desc(jaar), desc(samengevoegde_datvar)) %>% 
  group_by(patient_uri, jaar) %>% 
  fill(-matches("samengevoegde_datvar"), .direction = "up") %>% 
  mutate(samengevoegde_datvar = first(na.omit(samengevoegde_datvar))) %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, .keep_all = T)

laboratorium_ready <- laboratorium_clean %>% 
  mutate(jaar = year(labdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(labdat)) %>%
  group_by(patient_uri, jaar) %>%
  mutate(labdat_met_hba1c = labdat[which(!(labdat %in% NA) & ((hba1cmol %in% NA + hba1cperc %in% NA) < 2))][1]) %>% 
  fill(-matches("labdat_met_hba1c"), .direction = "up") %>%
  ungroup %>% 
  distinct(patient_uri, jaar, .keep_all = T) %>% 
  select(-labdat)

leefrisico_ready <- leefrisico_clean %>% 
  mutate(jaar = year(leefdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(leefdat)) %>%
  group_by(patient_uri, jaar) %>%
  fill(-matches("leefdat"), .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, .keep_all = T) %>% 
  select(-leefdat)

lichonderzoek_ready <- lichonderzoek_clean %>% 
  mutate(jaar = year(lichdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(lichdat)) %>%
  group_by(patient_uri, jaar) %>%
  fill(-matches("lichdat"), .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, .keep_all = T) %>% 
  select(-lichdat)

oogonderzoek_ready <- oogonderzoek_clean %>% 
  mutate(jaar = year(oogdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(oogdat)) %>%
  group_by(patient_uri, jaar) %>%
  fill(-matches("oogdat"), .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, .keep_all = T) %>% 
  select(-oogdat)

voetonderzoek_ready <- voetonderzoek_clean %>% 
  mutate(jaar = year(voetdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(voetdat)) %>%
  group_by(patient_uri, jaar) %>%
  fill(-matches("voetdat"), .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, .keep_all = T) %>% 
  select(-voetdat)

df <- patient_2020 %>% 
  right_join(
    intake_ready %>% 
      full_join(behandelaar_ready, by = c("patient_uri", "jaar")) %>% 
      full_join(laboratorium_ready, by = c("patient_uri", "jaar")) %>% 
      full_join(leefrisico_ready, by = c("patient_uri", "jaar")) %>% 
      full_join(lichonderzoek_ready, by = c("patient_uri", "jaar")) %>% 
      full_join(oogonderzoek_ready, by = c("patient_uri", "jaar")) %>% 
      full_join(voetonderzoek_ready, by = c("patient_uri", "jaar")) %>% 
      mutate(report_date = coalesce(samengevoegde_datvar, labdat_met_hba1c))
  )

df_voor_dashboarding <- df %>% 
  arrange(patient_uri, desc(jaar)) %>% 
  group_by(patient_uri) %>% 
  fill(c(datbez1,
         diagdat,
         diagnose_classification,
         agb,
         hoofdbhspec,
         insulinepen,
         pomp,
         glucsensor,
         insulinefreq,
         insuline_dagtotaal,
         insuline_basaaly,
         pomp_dagtotaal,
         pomp_basaal,
         pomp_bolus,
         tabakgebruikstatus,
         adviesroken,
         alcoholgebruikstatus)
       , .direction = "up") %>%
  ungroup %>% 
  filter(year(report_date) %in% c(2016:2021))

df_wetenschappelijk_onderzoek <- df %>% 
  arrange(patient_uri, desc(jaar)) %>% 
  group_by(patient_uri) %>% 
  fill(-matches("report_date"), .direction = "up") %>%
  ungroup %>% 
  filter(year(report_date) %in% c(2016:2020)) %>% 
  distinct(patient_uri, .keep_all = T)

#medicatie toevoegen
medicatie_ready <- intakesmetpturi %>%
  left_join(medicatiereg_clean, by = "intake_uri") %>%
  left_join(medicatie_clean, by = c("medicatieregistratie_uri")) %>%
  select(patient_uri, med, med_startdatum, med_einddatum, med_hoeveelheid) %>%
  drop_na(med) %>%
  filter(year(med_startdatum) <= 2021 | year(med_einddatum) >= 2016) %>%
  arrange(patient_uri, med, med_startdatum, desc(med_einddatum)) %>%
  distinct(patient_uri, med, .keep_all = T) %>%
  arrange(patient_uri, med_startdatum) %>%
  group_by(patient_uri) %>%
  mutate(number = row_number()) %>%
  ungroup() %>%
  pivot_wider(names_from = number,
              values_from = c(med, med_startdatum, med_einddatum, med_hoeveelheid)) %>%
  select(patient_uri, ends_with(paste0("_", 1:((dim(.)[2]-1)/4))))

df_voor_dashboarding %>% 
  left_join(medicatie_ready)

df_wetenschappelijk_onderzoek %>% 
  left_join(medicatie_ready)
