df <- rbind(mtcars, mtcars, mtcars, mtcars, mtcars, mtcars, mtcars, mtcars, mtcars, mtcars)
  

df$observed <- df$vs
casemixvars = c("mpg", " hp")
formula = paste("observed ~ ", paste(casemixvars, collapse = " + "))



cyl4 <- df[df$cyl == 4,]
cyl6 <- df[df$cyl == 6,]
cyl8 <- df[df$cyl == 8,]

mod <- glm(formula, data=cyl4, family="binomial")
cyl4$predicted <- predict(mod, cyl4, type="response")
cyl4$average <- mean(cyl4$observed)

mod <- glm(formula, data=cyl6, family="binomial")
cyl6$predicted <- predict(mod, cyl6, type="response")
cyl6$average <- mean(cyl6$observed)

mod <- glm(formula, data=cyl8, family="binomial")
cyl8$predicted <- predict(mod, cyl8, type="response")
cyl8$average <- mean(cyl8$observed)



sim1 <- rbind(cyl4, cyl8, cyl6)
sim1$predicted
sim1$average
sim1$cyl

sim2 <- df %>%
  group_by(cyl) %>%
  nest() %>%
  mutate(mod = data %>%  map(~glm(formula, data = ., family="binomial"))) %>%
  mutate(predicted = map2(mod, data, predict, type = "response")) %>%
  unnest(predicted, data) %>%
  group_by(cyl) %>%
  mutate(average = mean(observed, na.rm = T)) %>% 
  ungroup()


sim2$predicted
sim2$average
sim2$cyl

sort(sim1$predicted)== sort(sim2$predicted)
