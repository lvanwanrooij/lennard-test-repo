mtcars %>% 
  group_by(cyl) %>%
  do(quantile_tbl(.$mpg, c(0.25,0.75)))
library(dplyr)
library(gestalt)
library(tibble)

quantile_tbl <- quantile %>>>% enframe("quantile")

mtcars %>% 
  group_by(cyl) %>% 
  do({x <- .$mpg
  map_dfr(.x = c(.25, .5, .75),
          .f = ~ data_frame(Quantile = .x,
                            Value = quantile(x, probs = .x)))
  })
library(purrr)

mtcars %>% group_by(cyl) %>%
  summarise(`25%`=quantile(mpg, probs=0.25),
            `50%`=quantile(mpg, probs=0.5),
            `75%`=quantile(mpg, probs=0.75),
            avg=mean(mpg),
            n=n())
library(survminer)
install.packages("survminer")
