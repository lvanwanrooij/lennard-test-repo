devtools::load_all("H:/My Documents/dica-test-repository/syntax/apps/metagui")
allmeta <- metadatacheck()

check <- allmeta %>% 
  filter(year == 2020 & isCasemix == 1) 

df <- allmeta %>% 
  filter(year == 2020 & isCasemix == 1) %>% 
  mutate(corrected_for = ifelse(explanation3 == "", ifelse(explanation2 == "", explanation1, explanation2), explanation3),
         filename = paste0("H:/My Documents/dica-indicator-syntax/syntax/kpi-2020/", registry, "/", uri, ".R")) %>% 
  select(registry, order, uri, corrected_for) %>% 
  mutate(corrected_for = ifelse(uri == "dmtr-eerstejaars-overleving-systemisch", 'c("geslacht", "leeftijd", "mstad", "livermetas", "brainmetas", "who_cat", "organsites", "ldh", "brafmut")' , corrected_for)) %>% 
  mutate(corrected_for = ifelse(uri == "dmtr-eerstejaars-overleving", 'c("geslacht", "leeftijd", "mstad", "livermetas", "brainmetas", "who_cat", "organsites", "ldh", "brafmut")' ,corrected_for))

write.csv2(df, "casemixuitdraaivoorjanneke.csv")


myf <- function(x){
  lines <- readLines(x)
  lines <- lines[grepl("^casemixvars", lines)]
  gsub("")
}
myf(df$filename[10])
