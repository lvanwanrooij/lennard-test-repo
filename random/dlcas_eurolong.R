df

#in/exclusiecriteria
df <- df %>% 
  filter(age > 18)

#variabelen aanmaken
df <- df %>% 
  mutate(bmi = (lengte^2)/gewicht,
         age = ,
         male = ,
         ppoFEV1 = )

#eurolong1 en eurolong2 variabelen maken
df <- df %>% 
  mutate(eurolong1 = -2.821 +
           0.020 * age +
           0.455 * male +
           -0.015 * ppoFEV1 +
           0.206 * CAD +
           0.205 * CVD +
           0.660 * Thoracotomy +
           0.332 * extended_resection +
           0.214 * CKD,
         eurolong2 = -6.350 + 
           0.047 * age +
           0.889 * male +
           -0.055 * BMI +
           -0.010 * ppoFEV1 +
           0.892 * thoracotomy +
           0.983 * pneumonectomy)

exp(df$eurolong1)
exp(df$eurolong2)

exp(1)

df <- df %>% 
  mutate(risk_class_score = ifelse(age > 70, 1, 0) +
           ifelse(risk_class_score_continuous < 70, 2.5, 0) +
           ifelse(gender == "male", 2.5, 0) +
           )

df$risk_class_score_continuous 
df$risk_class_score_categories <- as.integer(cut(0:12, c(-1, 2.75, 5.25, 6.75, 7.75, 9.25, 13)))

df %>% 
  group_by(risk_class_score_categories) %>% 
  summarise(hoeveel_patienten = n(),
            hoeveel_overleden = sum(overleden == 1),
            mortaliteitspercentage = 100 * hoeveel_overleden / hoeveel_patienten)

            # lowerlimit = -qnorm(0.975) / sqrt(denominator * benchmark))

