x <- list.files("H:/My Documents/dica-indicator-syntax/syntax/kpi-2020/dlcal", full.names = T, recursive = T)
x <- x[c(9:12, 16:23)]
x

metagui::mygsubfilefunc(x, "doelplan = 2", "(doelplan = 2 OF (polioutcome = 3 EN overnamered = 2))", fixed = T)

metagui::mygsubfilefunc(x, "reggroep = 1 ", "reggroep = 1 EN (doelplan = 2 OF (polioutcome = 3 EN overnamered = 2))", fixed = T)



metagui::mygsubfilefunc(x, 'include_xr("xdoelplan", 0,
           df$doelplan == 1, 1,
           df$doelplan == 2, 2)',
'include_xr("xdoelplan", 0,
           df$doelplan == 1, 1,
           df$doelplan == 2, 2)

df %<>%
  mutate(polioutcome_overnamered = case_when(polioutcome == 3 & overnamered == 1 ~ 1,
                                             polioutcome == 3 & overnamered == 2 ~ 2,
                                             polioutcome == 3 & overnamered %in% c(1,2) ~ 3,
                                             TRUE ~ 0))',
                        fixed = T)


metagui::mygsubfilefunc(x, "df$xdoelplan == 2", "(df$xdoelplan == 2 | df$polioutcome_overnamered == 2)", fixed = T)



metagui::mygsubfilefunc(x, "select(patient_uri, uri, episode_id, ", "select(patient_uri, uri, episode_id, polioutcome, overnamered, ", fixed = T)
