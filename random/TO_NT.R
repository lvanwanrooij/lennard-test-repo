#### Includes ####

# install.packages(" ggplot2")
# install.packages ("dplyr")
# install.packages("cranlog")
# install.packages("eeptools")
# install.packages("tableone")
# 
# library(ggplot2)
# library(dplyr)
# library(cranlog)
# library(eeptools)
# library(tableone)


devtools::load_all("S:/Scripting&Onderwijs/3.staging/packages/dicapackage/")

## ------------------------------------------------------------------------------------------
#### Inladen RDS datasets (../../../input/dgoa-[dataset].[weeknummer/current].rds) ####
patientdica = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020.patient.rds");
patientiknl = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020_iknl.patient.rds");
preoperatiefdica = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020.preoperatief.rds");
preoperatiefiknl = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020_iknl.preoperatief.rds");
ovarbehandelingdica = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020.ovarbehandeling.rds");
ovarbehandelingiknl = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020_iknl.ovarbehandeling.rds");
ovarpathologiedica = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020.ovarpathologie.rds"); # dataset 2020 
ovarpathologieiknl = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020_iknl.ovarpathologie.rds"); # dataset 2020
chemotherapiedica = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020.chemotherapie.rds");
chemotherapieiknl= read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020_iknl.chemotherapie.rds");
followupdica = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020.patient.rds");
followupiknl = read_and_clean_rds("S:/Registraties/DGOA/2. Dataset/1. Data/2021-02-02/dgoa_2020_iknl.followup.rds");

#### Schonen datasets en merge ####
#### Alle POSIXct velden schonen #####
dflist <- list(patientdica = patientdica,
               patientiknl = patientiknl,
               preoperatiefdica = preoperatiefdica,
               preoperatiefiknl = preoperatiefiknl,
               ovarbehandelingdica = ovarbehandelingdica,
               ovarbehandelingiknl = ovarbehandelingiknl,
               ovarpathologiedica = ovarpathologiedica,
               ovarpathologieiknl = ovarpathologieiknl,
               chemotherapiedica = chemotherapiedica,
               chemotherapieiknl = chemotherapieiknl,
               followupdica = followupdica,
               followupiknl = followupiknl)

list2env(lapply(dflist, function(x) {
  dplyr::mutate_if(x, is.POSIXct, function(y){
    if_else(year(y) > year(today()) - 120 & year(y) <= year(today()), y, as.POSIXct(NA))
  })
}), envir = globalenv())

## Opschonen dataset ovarbehandeling, eerst filteren op stadiering zodat je alleen de patient met een stadiering over houdt.
ovarbehandelingdica2 <- ovarbehandelingdica %>%
  filter(ovarchiraard == 0)%>%
  arrange(patient_uri, ovarepisodedatum) %>%
  distinct(patient_uri,ovarepisodedatum,.keep_all = TRUE)
ovarbehandelingiknl2 <- ovarbehandelingiknl %>%
  filter(ovarchiraard == 0)%>%
  arrange(patient_uri, ovarepisodedatum) %>%
  distinct(patient_uri, ovarepisodedatum, .keep_all = TRUE)
#we laten ALLE stadieringen er in met een unieke episodedatum. (patienten met dubbele stadieringen bijven dus)


## Opschonen dataset preoperatief
preoperatiefdica2 <- preoperatiefdica %>%
  arrange(patient_uri, datpreopmdt, datbez0) %>%
  distinct(patient_uri, .keep_all = TRUE)
preoperatiefiknl2 <- preoperatiefiknl %>%
  arrange(patient_uri, datpreopmdt, datbez0) %>%
  distinct(patient_uri, .keep_all = TRUE)

## Opschonen dataset pathologie
ovarpathologiedica2 <- ovarpathologiedica %>%
  arrange(patient_uri, ovarhistdat) %>%
  distinct(patient_uri, .keep_all = TRUE)
ovarpathologieiknl2 <- ovarpathologieiknl %>%
  arrange(patient_uri, ovarhistdat) %>%
  distinct(patient_uri,.keep_all = TRUE)
# Hierhaal houd ik maar 1 pathologie reccord over.

# Opschonen patient dataset
patientdica2 <- patientdica %>%
  arrange(patient_uri, datovl) %>%
  distinct(patient_uri, .keep_all = TRUE)
patientiknl2 <- patientiknl %>%
  arrange(patient_uri, datovl) %>%
  distinct(patient_uri, .keep_all = TRUE)

#opschonen chemotherapie dataset
# we behouden elk reccord van een patient omdat we later gaan kijken waar adjuvante chemotherapie 
# heeft plaats gevonden

chemotherapiedica2 <- chemotherapiedica %>%
  arrange(patient_uri, chemoepisodedatum) %>%
  distinct(patient_uri,chemoepisodedatum, .keep_all = TRUE)
chemotherapieiknl2 <- chemotherapieiknl %>%
  arrange(patient_uri, chemoepisodedatum) %>%
  distinct(patient_uri,chemoepisodedatum, .keep_all = TRUE)

#followup dataset: arranged op descending zodat het laatste reccord van de patient overblijft en hierna uniek gemaakt. 
followupdica2 <- followupdica%>%
  arrange(patient_uri, desc(datovl))%>%
  distinct(patient_uri, .keep_all = TRUE)
followupiknl2 <- followupiknl%>%
  arrange(patient_uri, desc(datfollowup))%>%
  distinct(patient_uri, .keep_all = TRUE)

# Voorbereiding bronbestand
patientdica2 <-  patientdica2 %>%
  mutate(dataset_patient = "DICA")
patientiknl2 <-  patientiknl2 %>%
  mutate(dataset_patient = "IKNL")

### Stap 5: Appenden van datasets ####
patient <- bind_rows(patientdica2, patientiknl2)
ovarbehandeling <- bind_rows(ovarbehandelingdica2, ovarbehandelingiknl2)
preoperatief <- bind_rows(preoperatiefdica2, preoperatiefiknl2)
ovarpathologie <- bind_rows(ovarpathologiedica2, ovarpathologieiknl2)
chemotherapie <- bind_rows (chemotherapiedica2, chemotherapieiknl2)
followup <- bind_rows (followupdica2, followupiknl2)

#joinen left ovarbehandeling aan patient
beh_pat <- left_join(ovarbehandeling, patient, by = "patient_uri")
beh_pat_preop <- left_join(beh_pat, preoperatief, by = "patient_uri")
beh_pat_preop_path <- left_join(beh_pat_preop, ovarpathologie, by = "patient_uri")
beh_pat_preop_path_chem <- left_join(beh_pat_preop_path,  chemotherapie, by = "patient_uri") %>% 
  mutate(interval_ok_en_chemo = CalculateInterval(ovarepisodedatum, chemoepisodedatum, 0, 60, "days")) %>% 
  arrange(patient_uri, ovarepisodedatum, interval_ok_en_chemo) %>% 
  distinct(patient_uri, ovarepisodedatum, .keep_all = T) %>% 
  mutate(adjuvant = case_when(!is.na(interval_ok_en_chemo) ~ 0L,
                              !is.na(ovarepisodedatum) & !is.na(chemoepisodedatum) & is.na(interval_ok_en_chemo) ~1L, 
                              TRUE ~ NA_integer_))%>%
  mutate(jaarrap = year(ovarepisodedatum)) %>%
  filter(jaarrap >= 2013 & jaarrap <= year(today()))%>%
  filter(jaarrap %in% c(2015:2019)) # 1184

#Unieke patienten met bovenstaande join
uniekk<-beh_pat_preop_path_chem%>%
  arrange(patient_uri, ovarepisodedatum) %>%
  distinct(patient_uri,.keep_all = TRUE) # 1168 patienten

#beh_followup<- left_join(beh_pat_preop_path_chem, followup, by = "patient_uri")

#Exclusie criteria toepassen
dfexcl1<-beh_pat_preop_path_chem%>%
  filter(ovardiffgraad %in% c(0,1,2,3,4, NA)) # borderline eruit =928 over = 256 borderline

dfexcl2<-dfexcl1%>%
  filter(ovarhisttype == 0) # Alleen de epitheliale tumoren = 823
dfexcl4<- dfexcl2 %>%
  filter(epitheliaal4 %in% c(0,9,NA)) # exclusie van mucineuze carcinomen = 585

table(dfexcl4$ovarfigopa2014)
#0	nl	IA
#1	nl	IB
#2	nl	IC
#3	nl	IC1
#4	nl	IC2
#5	nl	IC3
#6	nl	IIA
#7	nl	IIB
#8	nl	IIIA
#9	nl	IIIA1
#10	nl	IIIA2
#11	nl	IIIB
#12	nl	IIIC
#13	nl	IVA
#14	nl	IVB


# Op 20-01 besproken om de patienten die een hoogstadium carcinoom hebben EN compleet zijn te behouden.
# Hiervoor moet ik NU alle stadium erin laten.
#dfexcl4<-dfexcl3%>%
#  filter(ovarfigopa %in% c(2,3,4,6)) # exclusie vn alle figo >2B = 469 over

uniekkk<-dfexcl4%>%
  arrange(patient_uri, ovarepisodedatum) %>%
  distinct(patient_uri,.keep_all = TRUE)



test<- beh_pat_preop_path_chem%>%
  dplyr::select(patient_uri, ovarepisodedatum, chemoepisodedatum, adjuvant, interval_ok_en_chemo)
test2<-beh_followup %>%
  dplyr::select(patient_uri, ovarepisodedatum, chemoepisodedatum, adjuvant, datfollowup)

test%>%
  count(patient_uri)%>%
  arrange(n)
#--------------------------------------------------#
# Figuur 1:
#--------------------------------------------------#
#populatie samen stellen voor flowchart.
#Hoeveel Unieke patienten zitten er in de DGOA tussen 2015-2019
ovarbehandelingdicauniek <- ovarbehandelingdica %>%
  arrange(patient_uri, ovarepisodedatum) %>%
  distinct(patient_uri,.keep_all = TRUE)
ovarbehandelingiknluniek <- ovarbehandelingiknl %>%
  arrange(patient_uri, ovarepisodedatum) %>%
  distinct(patient_uri,.keep_all = TRUE)
uniek <- bind_rows(ovarbehandelingdicauniek, ovarbehandelingiknluniek)%>%
  distinct (patient_uri, .keep_all= TRUE)%>%
  mutate(jaarrap = year(ovarepisodedatum)) %>%
  filter(jaarrap >= 2013 & jaarrap <= year(today()))%>%
  filter(jaarrap %in% c(2015:2019))
#uniek heeft het aantal unieke patienten in totaal van de df = 6453 patienten in totaal
# 714 patienten met stadiering, dit is opzich wel gek, maar zal met de join te maken hebben met het feit dat de arrange anders is.



#Variabele maken voor patienten met adnex/uterus extirpatie ook in vg:
# mogelijke combinaties zijn: ovaruterus ==1 +abdomenadnex2==1 OF ovaruterus ==2 + abdomenut==1
# eerst variabele omcoderen zodat NA bij waarde o worden geplaatst
dfexcl4$xovaruterus <- ifelse(dfexcl4$ovaruterus %in% c(1), 1, 0)
dfexcl4$xabdomenad2 <- ifelse(dfexcl4$abdomenad2 %in% c(1), 1, 0)
dfexcl4$vg <- ifelse((dfexcl4$xovaruterus ==1 & dfexcl4$xabdomenad2 ==1), 1, 0)

dfexcl4$yovaruterus <- ifelse(dfexcl4$ovaruterus %in% c(2), 1, 0)
dfexcl4$yabdomenut <- ifelse(dfexcl4$abdomenut %in% c(1), 1, 0)
dfexcl4$vg2 <- ifelse((dfexcl4$yovaruterus == 1 & dfexcl4$yabdomenut==1), 1, 0)

dfexcl4$zovaruterus <-ifelse(dfexcl4$ovaruterus %in%(3),1,0)

dfexcl4$uterusadnex <- ifelse((dfexcl4$vg ==1 | dfexcl4$vg2==1 | dfexcl4$zovaruterus ==1),1,0)


# Biopten en lymfklieren over d erijen bij elkaar optellen en clustere. Hiervoor nieuwe variabele aanmaken.
df8 <- dfexcl4 %>%
  mutate(biopt = ovarbiopt1 %in% 1 + ovarbiopt2 %in% 1 + ovarbiopt3 %in% 1 + ovarbiopt4 %in% 1 + ovarbiopt5 %in% 1 + ovarbiopt6 %in% 1 + ovarbiopt7 %in% c(1,8) + ovarbiopt8 %in% c(1,8)) %>%
  mutate(aorta = ovarlymf1 %in% 1 + ovarlymf2 %in%  1) %>%
  mutate( pelvienlinks = ovarlymf3 %in% 1 + ovarlymf4 %in% 1 + ovarlymf5 %in% 1 + ovarlymf9 %in% 1) %>%
  mutate (pelvienrechts = ovarlymf6 %in% 1 + ovarlymf7 %in% 1 + ovarlymf8 %in% 1 + ovarlymf10 %in% 1)%>%
  mutate (pelvien = ovarlymf1 %in% 1 + ovarlymf2 %in% 1 +ovarlymf3 %in% 1 +ovarlymf4 %in% 1 +ovarlymf5 %in% 1 +
            ovarlymf6 %in% 1 +ovarlymf7 %in% 1 +ovarlymf8 %in% 1 +ovarlymf9 %in% 1 +ovarlymf10 %in% 1)

df9 <- df8 %>% 
  group_by(patient_uri) %>% 
  summarise(bioptrij = sum(biopt), aortarij = sum(aorta), pelvienlinksrij = sum(pelvienlinks) , pelvienrechtsrij = sum(pelvienrechts),
            pelvienrij = sum(pelvien)) %>% 
  ungroup()
df10 <- left_join(df8, df9, by = "patient_uri")

# Compleet definitie= Ovaracites ==1 + uterusadnex ==1 + ovaromentum ==1 + bioptrij >=5 + ( (aortarij >=1 & pelvientij >=4) OF (ovarnumlym>10) )
df <- df10 %>%
  group_by(patient_uri) %>%
  mutate(compleet = case_when(any(ovarascites == 1) &
                                any(uterusadnex == 1) &
                                any(ovaromentum == 1) & 
                                bioptrij >= 5 & 
                                (( aortarij>=1 & pelvienrij >=4) |(ovarnumlym %in% c(10:999)))~ 1L,
                              TRUE ~ 0L)) %>%
  ungroup() %>%
  arrange(patient_uri, ovarepisodedatum, desc(compleet), desc(aortarij)) %>%
  distinct(patient_uri, .keep_all = TRUE)

table(df$compleet, useNA = 'always')


#FIlter patienten met compleet " hoog stadium", deze moeten weer terug naar de iclusie groep
table(df$ovarfigopa2014, df$compleet, useNA = 'always')
dfna <- df %>%
  filter (is.na(ovarfigopa)) # is 18x NA waaaronder 6 compleet NA




##-------------------------------------------------------------------------
# Final dataframe
##------------------------------------------------------------------------
# Dataframe aanmaken compleet + hoge FIGO + NA
#tm ovarfigo2014 0-6 = laagstadium 
  dffinal <- df%>%
  filter((compleet == 0 & ovarfigopa2014 %in% c(0,1,2,4,6, NA)) |(compleet == 1 & ovarfigopa2014 %in% c(0,1,2,4,6,7,8,11,12,14, NA)))



## Textbook outcome


#Variabele aanmaken voor de verschillende barplots in volgorde:

df$ovarascites_dichotoom <- df$ovarascites %in% 1 
df$ovaruterus_dichotoom <- df$uterusadnex %in% 1
df$ovaromentum_dichotoom <- df$ovaromentum %in% 1

df$xbioptrij <- ifelse(as.integer(df$bioptrij>=5),1,0)
df$ovarbiopt_samengesteld <- df$xbioptrij %in% 1

df$xpelvienaortarij <- ifelse(as.integer((df$aortarij>=1 & df$pelvienrij >=4) |(df$ovarnumlym %in% c(10:999))),1,0)
df$ovarlymf_pelvien <- df$xpelvienaortarij %in% 1


#volgorde
df$ovarascites_dichotoom
df$ovaruterus_dichotoom
df$ovaromentum_dichotoom
df$ovarbiopt_samengesteld
df$ovarlymf_pelvien
df$xcompleet <- (df$ovarascites_dichotoom + df$ovaruterus_dichotoom + df$ovaromentum_dichotoom + df$ovarbiopt_samengesteld + 
                   df$ovarlymf_pelvien) %in% 5
table(df$compleet)
table(df$xcompleet)

len1 <- df %>% 
  dplyr::select(ovarascites_dichotoom,
                ovaruterus_dichotoom,
                ovaromentum_dichotoom,
                ovarbiopt_samengesteld,
                ovarlymf_pelvien,
                xcompleet)

len1$evt_eerste_var_waardoor_niet_compleet <- ifelse(rowSums(len1) !=6, colnames(len1)[max.col(!(len1), "first")], "welcompleet")

table(len1$evt_eerste_var_waardoor_niet_compleet, useNA = "always")

len2 <- len1 %>% 
  gather(var, val, -evt_eerste_var_waardoor_niet_compleet) %>% 
  arrange(var) %>% 
  group_by(var) %>% 
  mutate(groepsgrootte = n(),
         aantalTRUE = sum(val),
         percentages = sum(val) / n()) %>% 
  mutate(perc_niet_compleet_baseline = 1,
         perc_niet_compleet_door_ovarascites_dichotoom = 1 - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:1]) / n(),
         perc_niet_compleet_door_ovaruterus_dichotoom = 1 - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:2]) / (n() - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:1] / n())),
         perc_niet_compleet_door_ovaromentum_dichotoom = 1 - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:3]) / (n() - sum(evt_eerste_var_waardoor_niet_compleet %in%c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:2] / n())),
         perc_niet_compleet_door_ovarbiopt_samengesteld = 1 - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:4]) / (n() - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:3] / n())),
         perc_niet_compleet_door_ovarlymf_pelvien = 1 - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:5]) / (n() - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:4] / n())),
         perc_niet_compleet_door_compleet = 1 - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:8]) / (n() - sum(evt_eerste_var_waardoor_niet_compleet %in% c("ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien")[1:5] / n()))
  ) %>%
  group_by(var) %>%
  slice(1) %>%
  ungroup() %>%
  dplyr::select(-evt_eerste_var_waardoor_niet_compleet)

lenbl <- len2[c(1),] %>% 
  mutate(var = "baseline",
         percentages = 1)

len2 <- lenbl %>% 
  bind_rows(len2) %>% 
  mutate(var = factor(var, levels = c("baseline", "ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien", "xcompleet"))) %>%
  arrange(var)

len3 <- len2 %>% 
  dplyr::select(starts_with("perc_niet")) %>% 
  slice(1) %>% 
  gather(var, val) %>% 
  arrange(-val)

len2 <- len2 %>%
  mutate(percentages = ifelse(var %in% c("baseline", "xcompleet"), 0, percentages))


ggplot(len2) + 
  geom_bar(aes(x = as.factor(var), y = percentages),    
           stat = "identity",
           position = position_dodge(),
           fill = "#d4e3db")+
  theme(panel.background = element_rect(fill = "white"),
        axis.text.x = element_text(family = "Corbel", colour = "#828585", angle= 90),
        axis.text.y = element_text(family = "Corbel", colour = "#828585"),
        axis.title = element_text(family = "Corbel", colour = "#828585"),
        axis.ticks = element_line(size = 1, colour = "#828585"),
        panel.grid.major.y = element_line(size = 0.5, linetype = "solid", colour = "#d1d1cc"),
        legend.title = element_text(family = "Corbel", size = 10, colour = "#828585"),
        legend.text = element_text(family = "Corbel", size = 9.5, colour = "#828585"),
        legend.position = "top", 
        legend.background = element_rect(fill = "#ffffff"),
        legend.key = element_rect(fill = "transparent"),
        plot.subtitle = element_text(family = "Corbel", colour = "#828585")) + 
  labs(subtitle = "Figure 2: Cumulative outcome of surgical staging for patients with ovarian cancer", x= "Surgical staging items", y = "Percentages (%)", fill = "") +
  scale_y_continuous(labels=scales::percent) +
  geom_line(aes(x = c(1:7), y = unlist(len3[1:7, 2]), linetype = "Cummulative percentage surgical items"))+
  scale_x_discrete(breaks = c("baseline", "ovarascites_dichotoom", "ovaruterus_dichotoom","ovaromentum_dichotoom", "ovarbiopt_samengesteld", "ovarlymf_pelvien", "xcompleet"),
                   labels = c("Patients with surgical staging", "Acites sampling" , "Uterus or adnexal extirption","Omentectomy", "Minimum of 5 biopsies\n of different regions", "Pelvic lymph node sampling", "Complete surgical staging")) + 
  theme(legend.title = element_blank())
