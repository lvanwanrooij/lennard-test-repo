daci <- list.files("H:/My Documents/dica-indicator-syntax/syntax/kpi-2020/daci/", full.names = T)
devtools::load_all("H:/My Documents/dica-test-repository/syntax/apps/metagui/")

mygsubfilefunc(daci,
'df %<>%
  mutate(comorbiditeit_cardiaal = as.integer(!pmax(comhypert, comap, commyo, comptca, comcabg, comkleplij, comklepver, comaf , comcarritme, comdecompcor, comhartfaal, comcarmyopath, na.rm = T) %in% 0),
         comorbiditeit_pulmonaal = as.integer(!pmax(comlong, compulmobstr, compulmfibr, na.rm = T) %in% 0))',
'df %<>%
  mutate(comorbiditeit_cardiaal = as.integer((!pmax(comhypert, comap, commyo, comptca, comcabg, comkleplij, comklepver, comaf , comcarritme, comdecompcor, comhartfaal, comcarmyopath, na.rm = T) == 0)),
         comorbiditeit_pulmonaal = as.integer((!pmax(comlong, compulmobstr, compulmfibr, na.rm = T) == 0)))', 
T)

mygsubfilefunc(daci,
'cardstatus_recoded = if_else(cardstatus %in% c(1:3), 1L, as.integer(cardstatus)),',
'cardstatus_recoded = if_else(cardstatus %in% c(1:3), 1L, if_else(cardstatus == 4, 9L, as.integer(cardstatus))),', T
)
