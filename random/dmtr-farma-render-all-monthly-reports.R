#2020
#Gemaakt door IA 
#--------------------------------------------------------------------------------------------------------------------------#
#### Nodige packages inladen #### DIt kan misschien ook in je eerste cleaning bestand... ligt er een beetje aan wat je wilt
#--------------------------------------------------------------------------------------------------------------------------#
library(rmarkdown)
library(knitr)
library(lubridate)
library(kableExtra)
library(tidyverse)
library(magrittr)
library(survival)
library(survminer)
library(gridExtra)
library(tableone)
library(extrafont)
Sys.setlocale("LC_TIME", "English")

#--------------------------------------------------------------------------------------------------------------------------#
#### Nodige bestanden sourcen ####
#--------------------------------------------------------------------------------------------------------------------------#
source("../../scripting/dmtr-farma-preprocessing.R", chdir = T)
#source("2.General_farma_calculations.R")
#source("3.Monthly_report_specific_calculations.R")

#--------------------------------------------------------------------------------------------------------------------------#
#### Eventueel ruimte voor algemene berekeningen ####
#--------------------------------------------------------------------------------------------------------------------------#


#--------------------------------------------------------------------------------------------------------------------------#
#### Render alle HTML bestanden ####
#--------------------------------------------------------------------------------------------------------------------------#

# BMS
render(input = "Reports/Monthly-report/DMTR-monthly-farma-report-BMS-html-zonder-tox.Rmd",
       output_file = "../../output/dmtr-monthly-farma-report-BMS-html-zonder-tox.html",
       output_format = "all", output_options = list())

# Novartis
render(input = "Reports/Monthly-report/DMTR-monthly-farma-report-Novartis-html-zonder-tox.Rmd",
       output_file = "../../../../../output/custom-2020/DMTR/dmtr-monthly-farma-report-Novartis-html-zonder-tox.html",
       output_format = "all", output_options = list())

# MSD
render(input = "DMTR-monthly-farma-report-MSD-html.Rmd",
       output_file = "../../output/dmtr-monthly-farma-report-MSD-html.html",
       output_format = "all", output_options = list())

# PF
render(input = "Reports/Monthly-report/DMTR-monthly-farma-report-PF-html-zonder-tox.Rmd", 
       output_file = "../../../../../output/custom-2020/DMTR/dmtr-monthly-farma-report-PF-html-zonder-tox.html",
       output_format = "all", output_options = list())


#--------------------------------------------------------------------------------------------------------------------------#
#### Render alle PDF bestanden ####
#--------------------------------------------------------------------------------------------------------------------------#

# Novartis
render(input = "Reports/Monthly-report/DMTR-monthly-farma-report-Novartis-pdf-zonder-tox.Rmd",
       output_file = "../../../../../output/custom-2020/DMTR/dmtr-monthly-farma-report-Novartis-pdf-zonder-tox.pdf",
       output_format = "pdf_document")

# MSD
render(input = "Reports/Monthly-report/DMTR-monthly-farma-report-MSD-pdf.Rmd",
       output_file = "../../../../../output/custom-2020/DMTR/dmtr-monthly-farma-report-MSD-pdf.pdf",
       output_format = "pdf_document")
