devtools::load_all("../../../packages/dicapackage")

get_outputtypes_from_indicators <- function(registry){
  
  scripts <- list.files(paste0("../../kpi-2020/", registry), recursive = T, full.names = T)
  indicatorscripts <- scripts[nchar(scripts) > 30 & !grepl("clinical-update-status.R|README", scripts)]
  
  run_most_of_indicatorscript <- function(indicatorscript) {
    
    indicatorscript <- scan(indicatorscript, what=character(), sep="\n", quiet=TRUE, encoding = "UTF-8")
    indicatorscript <- indicatorscript[!grepl("^saveRDS|^source", indicatorscript)]
    indicatorscript <- prepend(indicatorscript, "metadata <- NULL; inexclusielijst <- NULL; providerdf <- NULL; regiondf <- NULL; cidf <- NULL;")
    tc <- textConnection(indicatorscript)
    source(tc)
    close(tc)
    
    outputtypes_current_script <- list(metadata = metadata,
                                       inexclusielijst = inexclusielijst,
                                       providerdf = providerdf,
                                       regiondf = regiondf,
                                       cidf = cidf)
    
    outputtypes_current_script
  }

  outputs <- map(indicatorscripts, run_most_of_indicatorscript)
  names(outputs) <- sub("../../", "", indicatorscripts, fixed = T)
  # outputs <- map(outputs, function(x) x[lengths(x) > 0])
  outputs
}

dpca_outputs <- get_outputtypes_from_indicators("dpca")
dpca_outputs[[1]]

daci_outputs <- get_outputtypes_from_indicators("daci")
daci_outputs

registry <- "dpca"