#dicapack inladen
devtools::load_all("S:/Scripting&Onderwijs/3.staging/packages/dicapackage")

#datasets inlezen
# patient_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.patient.rds")
intake_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.intake.rds")
laboratorium_2019 =  read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.laboratorium.rds")
lichonderzoek_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.lichonderzoek.rds")
leefrisico_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.leefrisico.rds")
behandelaar_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.behandelaar.rds")
medicatiereg_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.medicatieregistratie.rds")
medicatie_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.medicatie.rds")
oogonderzoek_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.oogonderzoek.rds")
voetonderzoek_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.voetonderzoek.rds")
aandoeningen_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.aandoeningen.rds")
aandoening_2019 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2019.[2021-04-03].20210419/dpard_2019.aandoening.rds")
patient_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.patient.rds")
intake_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.intake.rds")
laboratorium_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.laboratorium.rds")
lichonderzoek_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.lichonderzoek.rds")
leefrisico_2020  = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.leefrisico.rds")
behandelaar_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.behandelaar.rds")
medicatiereg_2020 =  read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.medicatieregistratie.rds")
medicatie_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.medicatie.rds")
oogonderzoek_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.oogonderzoek.rds")
voetonderzoek_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.voetonderzoek.rds")
aandoeningen_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.aandoeningen.rds")
aandoening_2020 = read_and_clean_rds("S:/Registraties/DPARD/2. Dataset/1. Data/WA_TP2020.dpard_2020.[2021-04-03].20210419/dpard_2020.aandoening.rds")


#datasets binden
behandelaar <- bind_rows(behandelaar_2019, behandelaar_2020)
intake <- bind_rows(intake_2019, intake_2020)
laboratorium <- bind_rows(laboratorium_2019, laboratorium_2020)
leefrisico <- bind_rows(leefrisico_2019, leefrisico_2020)
lichonderzoek <- bind_rows(lichonderzoek_2019, lichonderzoek_2020)
oogonderzoek <- bind_rows(oogonderzoek_2019, oogonderzoek_2020)
voetonderzoek <- bind_rows(voetonderzoek_2019, voetonderzoek_2020)
medicatiereg <- bind_rows(medicatiereg_2019, medicatiereg_2020)
medicatie <- bind_rows(medicatie_2019, medicatie_2020)

#datasets cleanen
behandelaar_clean <- behandelaar

intake_clean <- intake

laboratorium_clean <- laboratorium %>% 
  mutate(hba1cmol = if_else(between(hba1cmol, 25, 150), hba1cmol, NA_integer_),
         hba1perc = if_else(between(hba1cperc * 10.93 - 23.5, 25, 150), hba1cperc, NA_real_))

leefrisico_clean <- leefrisico

lichonderzoek_clean <- lichonderzoek

oogonderzoek_clean <- oogonderzoek

voetonderzoek_clean <- voetonderzoek

medicatiereg_clean <- medicatiereg

medicatie_clean <- medicatie

#hulpdatasets aanmaken
intakesmetpturi <- patient_2020 %>% 
  right_join(intake_clean, by = "patient_uri") %>% 
  select(patient_uri, intake_uri)

laboratorium_jaren <- laboratorium_clean %>% 
  left_join(intakesmetpturi) %>% 
  mutate(labdat = if_else(is.na(hba1cmol) & is.na(hba1cperc), as.POSIXct(NA), labdat),
         jaar = year(labdat)) %>% 
  filter(jaar %in% c(2016:2021)) %>%
  select(patient_uri, jaar, labdat) %>% 
  arrange(patient_uri, desc(jaar), desc(labdat)) %>% 
  distinct(patient_uri, jaar, .keep_all = T)

intake_jaren <- intake_clean %>% 
  mutate(samengevoegde_datvar = coalesce(polibezdat, intakedat),
         jaar = year(samengevoegde_datvar)) %>% 
  filter(jaar %in% c(2016:2021)) %>% 
  select(patient_uri, jaar, samengevoegde_datvar) %>% 
  arrange(patient_uri, desc(jaar), desc(samengevoegde_datvar)) %>% 
  distinct(patient_uri, jaar, .keep_all = T)
  
rapportage_jaren <- intake_jaren %>% 
  full_join(laboratorium_jaren) %>% 
  mutate(report_date = coalesce(samengevoegde_datvar, labdat)) %>% 
  select(patient_uri, jaar, report_date)

#datasets bewerken tot 1 rij per patient per jaar
behandelaar_ready <- behandelaar_clean %>% 
  left_join(intakesmetpturi) %>% 
  select(-c(intake_uri, behandelaar_uri)) %>% 
  mutate(across(-behdat, as.character)) %>% 
  pivot_longer(-c(patient_uri, behdat)) %>% 
  mutate(jaar = year(behdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(behdat)) %>% 
  group_by(patient_uri, jaar, name) %>% 
  fill(value, .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, name, .keep_all = T) %>% 
  select(-behdat)

intake_ready <- intake_clean %>% 
  mutate(samengevoegde_datvar = coalesce(polibezdat, intakedat)) %>% 
  select(-c(intake_uri, polibezdat, intakedat)) %>% 
  mutate(across(-samengevoegde_datvar, as.character)) %>% 
  pivot_longer(-c(patient_uri, samengevoegde_datvar)) %>% 
  mutate(jaar = year(samengevoegde_datvar)) %>% 
  arrange(patient_uri, desc(jaar), desc(samengevoegde_datvar)) %>% 
  group_by(patient_uri, jaar, name) %>% 
  fill(value, .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, name, .keep_all = T) %>% 
  select(-samengevoegde_datvar)

laboratorium_ready <- laboratorium_clean %>% 
  left_join(intakesmetpturi) %>% 
  select(-c(intake_uri, laboratorium_uri)) %>% 
  mutate(across(-labdat, as.character)) %>% 
  pivot_longer(-c(patient_uri, labdat)) %>% 
  mutate(jaar = year(labdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(labdat)) %>% 
  group_by(patient_uri, jaar, name) %>% 
  fill(value, .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, name, .keep_all = T) %>% 
  select(-labdat)

leefrisico_ready <- leefrisico_clean %>% 
  left_join(intakesmetpturi) %>% 
  select(-c(intake_uri, leefrisico_uri)) %>% 
  mutate(across(-leefdat, as.character)) %>% 
  pivot_longer(-c(patient_uri, leefdat)) %>% 
  mutate(jaar = year(leefdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(leefdat)) %>% 
  group_by(patient_uri, jaar, name) %>% 
  fill(value, .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, name, .keep_all = T) %>% 
  select(-leefdat)

lichonderzoek_ready <- lichonderzoek_clean %>% 
  left_join(intakesmetpturi) %>% 
  select(-c(intake_uri, lichonderzoek_uri)) %>% 
  mutate(across(-lichdat, as.character)) %>% 
  pivot_longer(-c(patient_uri, lichdat)) %>% 
  mutate(jaar = year(lichdat)) %>% 
  arrange(patient_uri, desc(jaar), desc(lichdat)) %>% 
  group_by(patient_uri, jaar, name) %>% 
  fill(value, .direction = "up") %>% 
  ungroup %>% 
  distinct(patient_uri, jaar, name, .keep_all = T) %>% 
  select(-lichdat)

oogonderzoek_ready <- oogonderzoek_clean %>%
  left_join(intakesmetpturi) %>%
  select(-c(intake_uri, oogonderzoek_uri)) %>%
  mutate(oogonderzoek_geweest = 1) %>% 
  mutate(across(-oogdat, as.character)) %>%
  pivot_longer(-c(patient_uri, oogdat)) %>%
  mutate(jaar = year(oogdat)) %>%
  arrange(patient_uri, desc(jaar), desc(oogdat)) %>%
  group_by(patient_uri, jaar, name) %>%
  fill(value, .direction = "up") %>%
  ungroup %>%
  distinct(patient_uri, jaar, .keep_all = T) %>%
  select(-oogdat)

voetonderzoek_ready <- voetonderzoek_clean %>%
  left_join(intakesmetpturi) %>%
  select(-c(intake_uri, voetonderzoek_uri)) %>%
  mutate(across(-voetdat, as.character)) %>%
  pivot_longer(-c(patient_uri, voetdat)) %>%
  mutate(jaar = year(voetdat)) %>%
  arrange(patient_uri, desc(jaar), desc(voetdat)) %>%
  group_by(patient_uri, jaar, name) %>%
  fill(value, .direction = "up") %>%
  ungroup %>%
  distinct(patient_uri, jaar, name, .keep_all = T) %>% 
  select(-voetdat)

medicatie_ready <- intakesmetpturi %>%
  left_join(medicatiereg_clean, by = "intake_uri") %>%
  left_join(medicatie_clean, by = c("medicatieregistratie_uri")) %>%
  select(patient_uri, med, med_startdatum, med_einddatum, med_hoeveelheid) %>%
  drop_na(med) %>%
  filter(year(med_startdatum) <= 2021 | year(med_einddatum) >= 2016) %>%
  arrange(patient_uri, med, med_startdatum, desc(med_einddatum)) %>%
  distinct(patient_uri, med, .keep_all = T) %>%
  arrange(patient_uri, med_startdatum) %>%
  group_by(patient_uri) %>%
  mutate(number = row_number()) %>%
  ungroup() %>%
  pivot_wider(names_from = number,
              values_from = c(med, med_startdatum, med_einddatum, med_hoeveelheid)) %>%
  select(patient_uri, ends_with(paste0("_", 1:((dim(.)[2]-1)/4))))

#data samenvoegen in lang df
dflong <- bind_rows(behandelaar_ready,
                    intake_ready,
                    laboratorium_ready,
                    leefrisico_ready,
                    lichonderzoek_ready,
                    oogonderzoek_ready,
                    voetonderzoek_ready)

#data in wide format zetten
dfbreed <- dflong %>% 
  pivot_wider(names_from = name, values_from = value)

#patientset koppelen
df <- patient_2020 %>% 
  right_join(dfbreed, by = "patient_uri") %>% 
  left_join(rapportage_jaren, by = c("patient_uri", "jaar")) %>%
  drop_na(jaar)

#juiste patienten selecteren, distincten op meest recente jaar en variabelen vullen met meest recent gevulde waarde
#voor wetenschappelijk onderzoek Jessica
df_wetenschappelijk_onderzoek_oud <- df %>% 
  filter(jaar %in% c(2016:2019)) %>%
  arrange(patient_uri, desc(jaar)) %>% 
  group_by(patient_uri) %>% 
  fill(-matches("report_date"), .direction = "up") %>%
  drop_na(report_date) %>% 
  ungroup() %>% 
  distinct(patient_uri, .keep_all = T)

#juiste patienten selecteren en variabelen vullen met meest recent gevulde waarde
#voor dashboard scripting
df_voor_dashboarding_oud <- df %>% 
  filter(jaar %in% c(2016:2021)) %>%
  arrange(patient_uri, desc(jaar)) %>% 
  drop_na(report_date)

#medicatie joinen
df_wetenschappelijk_onderzoek %>% 
  left_join(medicatie_ready)

df_voor_dashboarding %>% 
  left_join(medicatie_ready)