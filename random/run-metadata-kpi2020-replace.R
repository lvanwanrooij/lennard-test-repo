#aannemende dat wd kpi-2020 is, worden zo alle files uit deze map gelist
files <- list.files(recursive = T, full.names = T) 

#bestanden die onderstaande strings bevatten excluderen
kpis2020 <- files[!grepl("clinical-update-status|soncos|codman-pack|kpi\\.R|README|Rproj|run-metadata", files)]

#slechts bestand die een dash (-) bevatten includeren, zo vallen daci.R, dasa.R etc. eruit
kpis2020 <- kpis2020[grepl("-", kpis2020)]




#normalizePath, omdat bij MRDM/DICA niet per se de repo op dezelfde locatie staat
kpis2020 <- normalizePath(kpis2020)

metagui::mygsubfilefunc(kpis2020,
                        'saveRDS\\(metadata(.+?);',
                        '',
                        F)

metagui::mygsubfilefunc(kpis2020,
                        'metadata\\["numeratorDefinition"\\](.+?)\n',
                        'metadata\\["numeratorDefinition"\\]\\1\nsaveRDS(metadata, paste0("../../../output/kpi-2020/", metadata[["uri"]], ".element-indicator-metadata.rds"));\n',
                        F)



#functie voor lezen metadata aanmaken
read_metadata <- function(x) {
  x <- scan(x, what=character(), sep="\n", quiet=TRUE, encoding = "UTF-8") #lines van script scannen
  x <- x[grepl("^metadata|^saveRDS\\(metadata", x)] #lines selecteren waarin metadata wordt aangemaakt + de regel waarbij deze wordt weggeschreven
  x <- gsub("../../../output", "../../output", x) #gsub omdat bij opslaan naar outputmap nu 1 map minder terug moet worden gegaan
  tc <- textConnection(x) #maak tekstconnectie aan
  source(tc) #run de syntax van de tekstconnectie
  close(tc) #sluit de textconnectie
}

#functie Vectorizen
read_metadata <- Vectorize(read_metadata)

#functie runnen over alle 2020 indicatorenscripts
read_metadata(kpis2020)
