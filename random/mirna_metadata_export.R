devtools::load_all("H:/My Documents/dica-test-repository/packages/dicatools/")

df <- metadatacheck(T)

df2021 <- df %>% 
  filter(year == 2021) %>% 
  mutate(casemixVars = if_else(isCasemix == 1, explanation3, "n.v.t.")) %>% 
  select(registry, uri, longLabel, casemixVars)


library(openxlsx)
wb <- createWorkbook()

walk(unique(df2021$registry), function(.x){
  addWorksheet(wb, .x)
  writeDataTable(wb, .x, df2021[df2021$registry == .x,])
  setColWidths(wb, .x, 1:4, c(5, 60, 60, 60))
  
})

saveWorkbook(wb, "H:/My Documents/mirnacheck.xlsx", overwrite = T)
