---
title: "Report `r format(Sys.Date(), '%Y')` | Patients | PF | yearly"
output:
  flexdashboard::flex_dashboard:
    css: ../documentatie/dica_style.css
    logo: ../documentatie/dica_logo.png
    orientation: columns
   
---

```{r child="../markdownchilds/child-0.0-general-initialisation.Rmd"}
```

```{r echo = FALSE}
year = year(today()) - 1
middel = c("Encorafenib and Binimetinib")
```

Overview
===================================================
Column {data-width=280}
---------------------------------------------------

### <b>Patient numbers </b>

```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-yearly-overview-patient-numbers-information-guide.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```
```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-all-overview-patient-numbers-table.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```

Column {.tabset data-width=360}
---------------------------------------------------
### <b> Patient diagnosed in `r year -4` </b> 
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2016), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-middle-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b>`r year -3` </b> 
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2017), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-middle-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b>`r year -2` </b>
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2018), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-middle-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b>`r year -1` </b>
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2019), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-middle-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b>`r year` </b>
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2020), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-middle-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b> Information guide </b>
```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-yearly-overview-middle-information-guide.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```

Column {.tabset data-width=360}
---------------------------------------------------
### <b> Treatments in `r year -4` </b> 
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2016), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-right-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b>`r year -3` </b> 
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2017), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-right-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b>`r year -2` </b> 
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2018), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-right-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b>`r year -1` </b> 
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2019), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-right-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b>`r year` </b> 
```{r, echo = F, results='asis'}
cat(unlist(pmap(list(table_year = 2020), function (table_year){knitr::knit_child('../markdownchilds/child-yearly-overview-right-tables.Rmd', envir = environment(), quiet = TRUE)})), sep = '\n')
```
### <b> Information guide </b>
```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-yearly-overview-right-information-guide.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```

No systemic therapy
===================================================

Column {data-width=330}
---------------------------------------------------

### No treatment
```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-yearly-no-systemic-therapy-no-treatment-information-guide.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```
```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-yearly-no-systemic-therapy-no-treatment-table.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```
Column {data-width=330}
---------------------------------------------------
### Local therapy only
```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-yearly-no-systemic-therapy-local-therapy-only-information-guide.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```
```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-yearly-no-systemic-therapy-local-therapy-only-table.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```
Column {data-width=330}
---------------------------------------------------

### Survival curve - no treatment
```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-yearly-no-systemic-therapy-no-treatment-survival-curve.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```
### Survival curve - local therapy only
```{r, echo = F, results='asis'}
cat(unlist({knitr::knit_child('../markdownchilds/child-yearly-no-systemic-therapy-local-therapy-only-survival-curve.Rmd', envir = environment(), quiet = TRUE)}), sep = '\n')
```
Patient characteristics {data-navmenu="`r middel[1]`"}
===================================================
Column {data-width=300}
---------------------------------------------------
### Information guide - patient characteristics

Column {.tabset}
---------------------------------------------------

### First line of treatment

### Second line of treatment

### Third line of treatment

### Fourth line of treatment

Tumor characteristics {data-navmenu="`r middel[1]`"}
===================================================

Column {data-width=300}
---------------------------------------------------
### Information guide - tumor characteristics

Column {.tabset}
---------------------------------------------------

### First line of treatment

### Second line of treatment

### Third line of treatment

### Fourth line of treatment

Toxicity {data-navmenu="`r middel[1]`"}
===================================================

Column {data-width=300}
---------------------------------------------------
### Information guide - toxicity

Column {.tabset}
---------------------------------------------------

### First line of treatment

### Second line of treatment

### Third line of treatment

### Fourth line of treatment

Treatment{data-navmenu="`r middel[1]`"}
===================================================

Column {data-width=300}
---------------------------------------------------
### Information guide - treatment

Column {.tabset data-width=380}
---------------------------------------------------
### First line of treatment

### Second line of treatment

### Third line of treatment

### Fourth line of treatment

Survival  {data-navmenu="`r middel[1]`"}
===================================================
 
Column {data-width=240}
----------------------------------------------------

### Information guide - survival 
 
Column {.tabset data-width=380}
---------------------------------------------------
### Overall survival - first line of treatment

### Second line of treatment

### Third line of treatment

### Fourth line of treatment

Column {.tabset data-width=380}
---------------------------------------------------
### Time to next treatment - first line of treatment

### Second line of treatment

### Third line of treatment

### Fourth line of treatment

