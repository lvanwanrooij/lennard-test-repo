# 2020-07-01
# concept versie voor data uitlevering MRDM aan externe onderzoekers
# DLCA-S l.vanderwoude@dica.nl


## ------------------------------------------------------------------------------------------
#### Inladen RDS datasets (../../input/dlcal-[dataset].[weeknummer/current].rds) ####
patient = readRDS("dlcas_2019_export_patient_final.rds");
verrichting = readRDS("dlcas_2019_export_verrichting_final.rds");
comorbiditeiten = readRDS("dlcas_2019_export_comorbiditeiten_final.rds");

## Alle POSIXct velden schonen ##
CleanDates(list(patient = patient,
                verrichting = verrichting,
                comorbiditeiten = comorbiditeiten), 120)

#####----------------------------------------------------------------------------------------------------#####
##### DATA CLEANING AND MERGE --------------------------------------------------------------------------------

patient <- patient %>%
  distinct(uri, .keep_all = TRUE) %>%
  rename(patient_uri = uri)

# Opschonen dataset verrichting
verrichting <- verrichting %>%
  arrange(patient_uri, reggroep, datbez1, datbez2, datdiag1, datdiag1beeld, datdiag2, datpreopmdt, datok) %>%
  distinct(patient_uri, reggroep, datbez1, datbez2, datdiag1, datdiag1beeld, datdiag2, datpreopmdt, datok, .keep_all = TRUE) %>%
  rename(verrichting_uri = uri)

#comorbiditeit casemix variabelen berekenen
comorbiditeiten %<>%
  mutate(comorbiditeit_cardiaal = as.integer(!pmax(comap, commyo, comptca, comcabg, comkleplij, comklepver, comaf, comcarritme, comdecompcor, comhartfaal, ccomcartranspl, na.rm = T) %in% 0),
         comorbiditeit_pulmonaal = as.integer(!pmax(comlong, compulmobstr, compulmfibr, compulmtranspl, na.rm = T) %in% 0),
         aantal_com_missings = rowSums(is.na(.))) %>%
  rename(comorbiditeiten_uri = uri)

comorbiditeiten$aantal_missings_comvars <- rowSums(is.na(comorbiditeiten[,match("comsetall1",
  colnames(comorbiditeiten)):match("commalig", colnames(comorbiditeiten))])) ==
  length(match("comsetall1", colnames(comorbiditeiten)):match("commalig", colnames(comorbiditeiten)))

# Stap 7: Mergen ####
df <- left_join(verrichting, patient, by = c("patient_uri")) %>%
  left_join(comorbiditeiten, by = c("patient_uri"))

df %<>%
  mutate(int_com_ok = CalculateInterval(datcom, datok, -365, 365, "days"),
         com_na_ok = int_com_ok > 0) %>%
  arrange(patient_uri, verrichting_uri, aantal_missings_comvars, com_na_ok, abs(int_com_ok), aantal_com_missings) %>%
  distinct(patient_uri, verrichting_uri, .keep_all = T)
