﻿dk <- data.frame(kleur = c("PMS 7697 100%", "PMS 7697 70%", "PMS 7697 40%", "PMS 1655 100%",
                                    "PMS 566 100%", "PMS 566 50%",
                                    "PMS 611 100%", "PMS 611 50%", "PMS 611 30%",
                                    "PMS 441 100%",  "PMS 444 100%",  "PMS 446 100%"),
                          hex = c("#4E87A0", "#4E87A0B2", "#4E87A066", "#FC4C02",
                                  "#D4E3DB", "#D4E3DB80",
                                  "#D6CF40", "#D6CF4080", "#D6CF404C",
                                  "#D1D1CC", "#828585FF", "#404545FF"),
                          stringsAsFactors = F)

dk$base <- grepl("100", dk$kleur)

# dk <- dk %>% 
#   arrange(-base, kleur)   

bp <- barplot(rep(1, 12), rep(2, 12), col = rev(dk$hex), horiz = T, axes = F)


text(0.1, bp, labels = rev(1:12))
text(0.25, bp, labels = rev(dk$kleur))
text(0.75, bp, labels = rev(dk$hex))

theme_dica <- theme_bw() +
  theme(axis.line.x = element_line(colour = dk$hex[12]),
        axis.title = element_text(colour = dk$hex[11]),
        panel.grid.major.x = element_blank(),
        panel.grid.major.y = element_line(colour = dk$hex[10]),
        panel.grid.minor.x = element_blank(),
        panel.grid.minor.y = element_line(colour = dk$hex[10]),
        panel.border= element_blank(),
        plot.margin = unit(c(1,1,1,0), "cm"),
        text = element_text(family = "Corbel"),
        complete  = F
  )

# text = element_text(family = "Times")

ggplot(df, aes(x = geslacht)) +
  geom_bar() +
  xlab("aantal patiënten") +
  ylab("geslacht") +
  theme_dica +
  scale_y_continuous(expand = expand_scale(0, 0))
  
#install.packages("extrafont")
library(extrafont)
#import_fonts()
#loadfonts(device = 'win')
