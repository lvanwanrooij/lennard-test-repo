x <- list.files("H:/My Documents/dica-indicator-syntax/output/kpi-2020", full.names = T)
x <- x[grep("provider", x)]

y <- lapply(x, readRDS)
sapply(y, colnames)

inexclusielijst_df = inexclusielijst
metadata_visual = "provider-count-table"


sapply(y, "[[", "count")

inexclusielijst %>% 
  count(provider, year, name = "count") %>% 
  select(count) %>% 
  pull %>% 
  table
  complete(provider, year, fill = list(count = 0)) 

  

include_xr

file <- "H:/My Documents/dica-indicator-syntax/syntax/kpi-2020/dlcal/dlcal-inzet-immunotherapie-onder-70.R"

myf <- function(file){
  lines <- paste(readLines(file, encoding = 'UTF-8'), collapse = "\n")
  before_includexr = gsub('(.*)(include_xr\\(\")(.+?)(\")(.+?)(\\)\n\n)(.*)', '\\1', lines)
  after_includexr = gsub('(.*)(include_xr\\(\")(.+?)(\")(.+?)(\\)\n\n)(.*)', '\\7', lines)
  string <- gsub("[\n\r]", '', gsub('(.*include_xr\\(\")(.+?)(\")(.+?)(\\)\n\n.*)', '\\2\\4', lines))
  splitted = unlist(strsplit(string, ", "))
  varname = splitted[1]
  standaardwaarde = splitted[2]
  condities = gsub("df\\$", "", splitted[-c(1:2)][seq_along(splitted[-c(1:2)]) %% 2 == 1])
  condities[1] <- trimws(condities[1])
  # aantal_benodigde_spaties = paste(rep("", nchar("mutate(case_when(") + nchar(varname)), collapse = " ")
  aantal_benodigde_spaties = paste(rep("", nchar("mutate(case_when(") + nchar(varname) - 5), collapse = " ")
  condities[-1] <- paste0(aantal_benodigde_spaties, condities[-1])
  waarden = paste0(splitted[-c(1:2)][seq_along(splitted[-c(1:2)]) %% 2 == 0], "L") 
  condsenvals = paste(condities, "~", waarden)
  collapsed = paste(condsenvals, collapse = ",\n")
  complete_casewhen = paste0("df %<>%\n  mutate(", varname, "= case_when(", collapsed, ",\n", aantal_benodigde_spaties, "          TRUE ~", standaardwaarde, "L))\n")
  complete_casewhen = gsub("NAL\\)", "NA_integer_)", complete_casewhen)
  complete_script = paste(before_includexr, complete_casewhen, after_includexr, sep = "\n")
  writeLines(enc2utf8(complete_script), file, useBytes = T)
}

myf(file)


myf <- function(...){
  arglist <- match.call(expand.dots = FALSE)$...
  # varname = arglist[[1]]
  # conditions <- arglist[seq(3, length(arglist) - 1, 2)]
  # values <- unlist(arglist[seq(4, length(arglist), 2)])
  # list(varname, conditions, values)
  arglist
}
myf(string)

xr_lines <- grep("include_xr", x2)[1]

i <- 0
while(FALSE){
  i <- i + 1
lengths(regmatches(lines[xr_lines:xr_lines+i], gregexpr("\\(", lines[xr_lines:xr_lines+i]))) !=
lengths(regmatches(lines[xr_lines:xr_lines+i], gregexpr("\\)", lines[xr_lines:xr_lines+i])))
}
i

x3
lengths(regmatches(x2, gregexpr("\\(", x2)))
lengths(regmatches(x2, gregexpr("\\)", x2)))


while(FALSE){
  sum(gregexpr("\\(", x2[x3])[[1]] > 0) != sum(gregexpr("\\)", x2[x3])[[1]] > 0)
}

x2
sapply(x2, str_count, "\\(")

gregexpr("message", x2)



mgsub(x2, '(.+?)(include_xr)(.+?)(\n\n)', '\\3')

?mgsub
library(mgsub)
strsplit(x2, "include_xr")

?strsplit

x <- "123456789"

gsub("(123(23))", '\\2', x)
à