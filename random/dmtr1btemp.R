#####----------------------------------------------------------------------------------------------------#####
##### CREATING METADATA LIST ---------------------------------------------------------------------------------
metadata = list();
metadata["version"] = "2.0.0";
metadata["registry"] = "dmtr";
metadata["year"] = "2020";
metadata["uri"] = "dmtr-aantal-patienten-behandeld";
metadata["type"] = "count-indicator";
metadata["visual"] = "provider-count-table";
metadata["order"] = "1B";
metadata["isidCode"] = "ISID000000";
metadata["inidCode"] = "INID000000";
metadata["shortLabel"] = "Aantal patiënten";
metadata["longLabel"] = "Aantal  palliatief behandelde irresectabel IIIC/IV patiënten onder behandeling bij een melanoomcentrum";
metadata["chapter"] = "Procesindicatoren";
metadata["isExternal"] = "1";
metadata["startMonth"] = "1";
metadata["monthCount"] = "12";
metadata["isCasemix"] = "0";
metadata["denominatorLabel"] = "";
metadata["denominatorCondition"] = "";
metadata["numeratorLabel"] = "Aantal  palliatief behandelde irresectabel IIIC/IV patiënten onder behandeling bij een melanoomcentrum";
metadata["numeratorCondition"] = "";
metadata["explanation1"] = "Het aantal patiënten wordt over alle registratiejaren berekend. Hierbij is het aantal patiënten onafhankelijk van de type behandeling die de patiënt heeft ondergaan.";
metadata["explanation2"] = "";
metadata["explanation3"] = "";
metadata["isTransparent"] = "1";
metadata["casemixFactors"] = "";
metadata["casemixVariables"] = "";
metadata["excluded"] = "";
metadata["isDICA"] = "1";
metadata["isIGJ"] = "0";
metadata["denominatorDefinition"] = ""
metadata["numeratorDefinition"] = "datovl = missing EN sysadj = missing"

saveRDS(metadata, paste0("../../../output/kpi-2020/", metadata[["uri"]], ".element-indicator-metadata.rds"));

#####----------------------------------------------------------------------------------------------------#####
##### LOAD DICA-PACK FUNCTIONS, READ RDS DATASETS AND RUN CLEANDATES FUNCTION --------------------------------
source("./dmtr.R");

patient = readRDS("../../../input/dmtr_2019_patient.current.rds")
registratie = readRDS("../../../input/dmtr_2019_registratie.current.rds")
episode = readRDS("../../../input/dmtr_2019_episode.current.rds")

## Alle POSIXct velden schonen ##
CleanDates(list(patient = patient,
                registratie = registratie,
                episode = episode), 120)

#####----------------------------------------------------------------------------------------------------#####
##### DATA CLEANING AND MERGE --------------------------------------------------------------------------------
patient %<>%
  rename(patient_uri = uri)

registratie %<>%
  rename(registratie_uri = uri)

episode %<>%
  rename(episode_uri = uri)

df <- registratie %>% 
  left_join(patient, by = "patient_uri") %>% 
  right_join(episode, by = "registratie_uri") %>% 
  filter(sysadj %in% c(0, 1, 9)) %>% 
  arrange(patient_uri, episodevolgnr)

#####----------------------------------------------------------------------------------------------------#####
##### EVT AANMAKEN EXTRA VARIABELEN EN RUNNEN STANDAARD FUNCTIES ---------------------------------------------
df %<>%
  mutate(episodejaar = ReportingYear(datbez1, 2013, month_threshold = 7),
         braf_ipi = case_when(date(startipi) < date("2013-07-01") & date(startipi) > date("2012-07-01") ~ 1L,
                              date(startbraf) < date("2013-07-01") & date(startbraf) > date("2012-07-01") ~ 1L,
                              TRUE ~ 0L)) %>%
  group_by(patient_uri) %>%
  mutate(braf_ipi_max = max(braf_ipi),
         beginjaar = case_when(braf_ipi_max == 0 ~ as.integer(min(episodejaar, na.rm = T)),
                               braf_ipi_max == 1 ~ 2013L)) %>%  
  ungroup() %>%
  arrange(patient_uri, beginjaar) %>%
  distinct(patient_uri, .keep_all = T) %>% 
  mutate(beginjaar = if_else(beginjaar == 2013 & date(datbez1) < date("2013-07-01") & braf_ipi_max == 0, NA_integer_, beginjaar))

df$eindjaar <- coalesce(ReportingYear(df$datovl, 2012, month_threshold = 7),
                         as.integer(ifelse(month(today()) <= 6, year(today()), year(today()) + 1)))
                                                 
df$id_fusie <- fusie_hospital_id("dmtr", df$registratie_id)
df$jaarrap <- df$beginjaar

df %<>% 
  drop_na(beginjaar, eindjaar) %>% 
  # complete(patient_uri, jaarrap) %>% 
  group_by(patient_uri) %>% 
  complete(jaarrap = full_seq(2013:year(today()), 1)) %>% 
  mutate(beginjaar = min(beginjaar, na.rm = T),
         eindjaar = max(eindjaar, na.rm = T),
         id_fusie = max(id_fusie, na.rm = T),
         upn = max(upn, na.rm = T),
         gebdat = max(gebdat, na.rm = T)) %>% 
  ungroup() %>% 
  filter(jaarrap >= beginjaar & jaarrap <= eindjaar) %>%  
  mutate(teller = 1)

#SELECT RELEVANT VARIABLES
df <- df %>%
  select(patient_uri, upn, gebdat, teller = starts_with("teller"), id_fusie, jaarrap)

#####----------------------------------------------------------------------------------------------------#####
##### (1) CREATE INEXCLUSIONLIST, (2) AGGREGRATE ON PROVIDER AND REGION LEVEL AND (3) SAVE OUTPUT AS RDS ----- 
#inexclusielijst maken
inexclusielijst <- create_inex_global_2020(metadata$type)
#inexclusielijst aggregeren naar ziekenhuisniveau
providerdf <- aggregate_inexclusion_list_on_provider_level(inexclusielijst, metadata[["visual"]])
#inexclusielijst aggregeren naar landelijk niveau
regiondf <- aggregate_inexclusion_list_on_region_level(inexclusielijst, metadata[["visual"]])

#Wegschrijven output
saveRDS(inexclusielijst, paste0("../../../output/kpi-2020/", metadata[["uri"]], ".patient-indicator-basic-inclusion-list.rds"));
saveRDS(providerdf, paste0("../../../output/kpi-2020/", metadata[["uri"]], ".provider-count-indicator.rds"));
message("OK");
