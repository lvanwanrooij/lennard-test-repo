library(tidyverse)
library(zoo)
pdfs <- list.files("S:/Indicatoren/2. Externe definitieve gidsen/Definitieve gidsen 2020/PDF", pattern = "pdf", full.names = T)

extract_pdf_data <- function(file, pagenumber){
  
  pdfpagina <- pdftools::pdf_data(file)[[pagenumber]]
  
  links <- pdfpagina %>% 
    filter(x < 213) %>% 
    group_by(y) %>% 
    mutate(concatted_line = paste(text, collapse = " ")) %>% 
    ungroup %>% 
    select(x, y, concatted_line)
  
  rechts <- pdfpagina %>% 
    filter(x >= 213) %>% 
    group_by(y) %>% 
    mutate(concatted_line = paste(text, collapse = " ")) %>% 
    ungroup %>% 
    select(x, y, concatted_line)
  
  df <- rechts %>% 
    left_join(links, by = "y", suffix = c("_rechts", "_links")) %>% 
    distinct(concatted_line_rechts, .keep_all = T) %>% 
    mutate(concatted_line_links = na.locf(concatted_line_links)) %>% 
    group_by(concatted_line_links) %>% 
    mutate(concatted_sentences = paste(concatted_line_rechts, collapse = " ")) %>% 
    ungroup() %>% 
    distinct(concatted_line_links, .keep_all = T) %>% 
    select(key = concatted_line_links, val = concatted_sentences) %>% 
    mutate(key = case_when(key == "Informatie voor" ~ "Informatie voor cliënten",
                           key == "Bron (achtergrond)" ~ "Bron (achtergrond) van de indicator",
                           key == "Databron" ~ "Databron (registratie)",
                           TRUE ~ key)) %>% 
    #vul onderstaande regel aan met eventuele andere relevante velden
    filter(grepl("Indicatornaam|Indicatornummer|Operationalisatie|Informatie voor cliënten|Transparantie|Type indicator|Relevantie|Datatype|Bron (achtergrond) van de indicator|Teller|Noemer|Casemix|Antwoordopties|In-/exclusiecriteria|Meetperiode|Aanleverfrequentie|Aanleverniveau", key))
  
  df
}

allpages <- function(file){
  indicatorstart <- 5
  indatorend <- pdftools::pdf_data(file)[[2]] %>% filter(lag(text) == "Wijzigingstabel") %>% pull(text) %>% as.integer - 1
  bind_rows(lapply(indicatorstart:indatorend, function(x) extract_pdf_data(file, x))) %>% 
    mutate(order = lead(ifelse(key == "Indicatornummer", val, NA_character_)),
           order = na.locf(order)) %>% 
    relocate(order) %>% 
    filter(key == "Relevantie")
}

allpages(pdfs[6])


#voor nu even opgezet met dat ie naar paginanummer kijkt. vul hier bij 5:9 de paginanummers in van de pdf waar de tabellen met indicatoreninfo staan. bij daci is dat 5 t/m 9
daci <- bind_rows(lapply(5:9, function(x) extract_pdf_data(pdfs[1], x))) %>% 
  mutate(order = lead(ifelse(key == "Indicatornummer", val, NA_character_)),
         order = na.locf(order)) %>% 
  relocate(order) %>% 
  filter(key == "Relevantie")


#voor nu even opgezet met dat ie naar paginanummer kijkt. vul hier bij 5:12 de paginanummers in van de pdf waar de tabellen met indicatoreninfo staan. bij dasa is dat 5 t/m 12
dasa <- bind_rows(lapply(5:12, function(x) extract_pdf_data(pdfs[2], x))) %>% 
  mutate(order = lead(ifelse(key == "Indicatornummer", val, NA_character_)),
         order = na.locf(order)) %>% 
  relocate(order) %>% 
  filter(key == "Relevantie")

dbir <- bind_rows(lapply(5:8, function(x) extract_pdf_data(pdfs[3], x))) %>% 
  mutate(order = lead(ifelse(key == "Indicatornummer", val, NA_character_)),
         order = na.locf(order)) %>% 
  relocate(order) %>% 
  filter(key == "Relevantie")

dato <- bind_rows(lapply(14, function(x) extract_pdf_data(pdfs[4], x))) %>% 
  mutate(order = lead(ifelse(key == "Indicatornummer", val, NA_character_)),
         order = na.locf(order)) %>% 
  relocate(order) %>% 
  filter(key == "Relevantie")

dcra <- bind_rows(lapply(5:14, function(x) extract_pdf_data(pdfs[5], x))) %>% 
  mutate(order = lead(ifelse(key == "Indicatornummer", val, NA_character_)),
         order = na.locf(order)) %>% 
  relocate(order) %>% 
  filter(key == "Relevantie")

dgoa <- bind_rows(lapply(5:10, function(x) extract_pdf_data(pdfs[6], x))) %>% 
  mutate(order = lead(ifelse(key == "Indicatornummer", val, NA_character_)),
         order = na.locf(order)) %>% 
  relocate(order) %>% 
  filter(key == "Relevantie")

dhba <- bind_rows(lapply(5:9, function(x) extract_pdf_data(pdfs[7], x))) %>% 
  mutate(order = lead(ifelse(key == "Indicatornummer", val, NA_character_)),
         order = na.locf(order)) %>% 
  relocate(order) %>% 
  filter(key == "Relevantie")

pdftools::pdf_info(pdfs[1])$pages


library(purrr)
map2(pdfs, 3, ~ extract_pdf_data)

pmap(list(pdfs, 5), function(x, y) extract_pdf_data(x,y))



get_order_and_relevance <- function(pdf){
  df <- bind_rows(pdftools::pdf_data(pdf), .id = "pagenr") %>% 
    mutate(across(pagenr, as.integer)) %>% 
    arrange(pagenr, y, x) %>% 
    group_by(pagenr, y) %>% 
    mutate(concatted_line = paste(text, collapse = " ")) %>% 
    ungroup() %>% 
    filter(grepl("Indicatornummer|Relevantie", concatted_line)) %>% 
    mutate(order = ifelse(grepl("Indicatornummer", concatted_line), sub("(Indicatornummer )(.*)", "\\2", concatted_line), NA_character_),
           relevance = ifelse(grepl("Relevantie", concatted_line), sub("(Relevantie )(.*)", "\\2", concatted_line), NA_character_),
           order = na.locf(order),
           relevance = na.locf(relevance, fromLast = T)) %>% 
    arrange(order) %>% 
    distinct(order, .keep_all = T) %>% 
    select(order, relevance)
  df
}

df2 <- df %>% 
  mutate(grp = cumsum(text == "Datatype"))

extract_relevance <- function(pdf){
  df <- bind_rows(pdftools::pdf_data(pdf), .id = "pagenr") %>% 
    mutate(across(pagenr, as.integer)) %>% 
    arrange(pagenr, y, x) %>% 
    group_by(pagenr, y) %>% 
    mutate(concatted_line = paste(text, collapse = ";;;")) %>% 
    ungroup() %>% 
    mutate(linenr = row_number()) %>% 
    filter(cumsum(concatted_line == "3;;;Indicatoren") == 2 & cumsum(concatted_line == "4;;;Wijzigingstabel") < 2) %>% 
    slice(2:(nrow(.) - 1)) %>% 
    mutate(order = cumsum(text == "Indicatornaam")) %>% 
    group_by(order) %>% 
    summarise(concatted_indicator = paste(text, collapse = ";;;")) %>% 
    ungroup %>% 
    mutate(relevance = trimws(gsub(";;;", " ", gsub("(.*)(Relevantie)(.+?)(Datatype)(.*)", "\\3", concatted_indicator)))) %>% 
    select(order, relevance)
  df
}


bind_rows(lapply(pdfs, extract_relevance))

extract_relevance(pdfs[10])



links <- df %>% 
  filter(x < 213)
rechts <- df %>% 
  filter(x >= 213)


pdf = pdfs[1]


get_order_and_relevance(pdfs[1])


extract_pdf_data <- function(file){
  
  pdfpagina <- bind_rows(pdftools::pdf_data(file), .id = "pagenr")
  
  pdfpagina <- pdfpagina %>% 
    filter(between(pagenr, pdfpagina %>% filter(text == "Indicatoren") %>% slice(2) %>% pull(pagenr), pdfpagina %>% filter(text == "Wijzigingstabel") %>% slice(2) %>% pull(pagenr) %>% as.integer() - 1))
  
  links <- bind_rows(pdfpagina) %>% 
    filter(x < 213) %>% 
    group_by(y) %>% 
    mutate(concatted_line = paste(text, collapse = " ")) %>% 
    ungroup %>% 
    select(x, y, concatted_line)
  
  rechts <- bind_rows(pdfpagina)  %>% 
    filter(x >= 213) %>% 
    group_by(y) %>% 
    mutate(concatted_line = paste(text, collapse = " ")) %>% 
    ungroup %>% 
    select(x, y, concatted_line)
  
  df <- rechts %>% 
    left_join(links, by = "y", suffix = c("_rechts", "_links")) %>% 
    distinct(concatted_line_rechts, .keep_all = T) %>% 
    mutate(concatted_line_links = na.locf(concatted_line_links)) %>% 
    group_by(concatted_line_links) %>% 
    mutate(concatted_sentences = paste(concatted_line_rechts, collapse = " ")) %>% 
    ungroup() %>% 
    distinct(concatted_line_links, .keep_all = T) %>% 
    select(key = concatted_line_links, val = concatted_sentences) %>% 
    mutate(key = case_when(key == "Informatie voor" ~ "Informatie voor cliënten",
                           key == "Bron (achtergrond)" ~ "Bron (achtergrond) van de indicator",
                           key == "Databron" ~ "Databron (registratie)",
                           TRUE ~ key)) %>% 
    #vul onderstaande regel aan met eventuele andere relevante velden
    filter(key %in% c("Indicatornaam", "Indicatornummer", "Operationalisatie", "Informatie voor cliënten", "Transparantie", "Type indicator", "Relevantie", "Datatype", "Bron (achtergrond) van de indicator", "Teller", "Noemer", "Casemix", "Antwoordopties", "In-/exclusiecriteria", "Meetperiode", "Aanleverfrequentie", "Aanleverniveau") | grepl("Teller|Noemer", key)) %>%   
    mutate(order = ifelse(key == "Indicatornummer", val, NA_character_),
           order = na.locf(order)) %>% 
    relocate(order) 
  
  df
}








extract_pdf_data(pdfs[2])


df <- extract_pdf_data(pdfs[2])
identical(df %>% filter(key == "Relevantie"), dasa)


bind_rows(pdftools::pdf_data(pdfs[12]), .id = "pagenr") %>% filter(text == "Wijzigingstabel")

x %>% filter(text == "Indicatoren")


c(pdfpagina %>% filter(text == "Indicatoren") %>% slice(1) %>% pull(pagenr) %>% as.integer(), pdfpagina %>% filter(text == "Wijzigingstabel") %>% slice(2) %>% pull(pagenr) %>% as.integer() - 1)  

pdfpagina %>% 
  filter(between(pagenr, pdfpagina %>% filter(text == "Indicatoren") %>% slice(1) %>% pull(pagenr), pdfpagina %>% filter(text == "Wijzigingstabel") %>% slice(2) %>% pull(pagenr) %>% as.integer() - 1)) %>% 
  distinct(pagenr)

         