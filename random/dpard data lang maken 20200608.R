#### Script DPARD data voor onderzoek###
syntax_datum <- "2020-05-25"
syntax_versie <- "1.0"

#--------------------------------------------------------------------------------------------------------------------------#
# Libraries inladen ####
#--------------------------------------------------------------------------------------------------------------------------#

source("S:/Registraties/ALGEMEEN/GIT repository gedeeld met MRDM/9. AOwerkmap/dica-pack-V1.0.0.R")
source("S:/Registraties/ALGEMEEN/GIT repository gedeeld met MRDM/2. master/syntax/kpi-2020/codman-pack.R")

patient <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_patient_final.rds")
intake <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_intake_final.rds")
laboratorium <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_laboratorium_final.rds")
lichond <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_lichonderzoek_final.rds")
leefrisico  <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_leefrisico_final.rds")
behandelaar <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_behandelaar_final.rds")
medicatiereg <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_medicatieregistratie_final.rds")
medicatie <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_medicatie_final.rds")
oogonderzoek <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_oogonderzoek_final.rds")
voetonderzoek <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_voetonderzoek_final.rds")
aandoeningen <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_aandoeningen_final.rds")
aandoening <- readRDS("S:/Registraties/DPARD/2. Dataset/1. Data/2020-05-12/dpard_2019_export_aandoening_final.rds")

## ------------------------------------------------------------------------------------------
#### Alle POSIXct velden schonen ####
## ------------------------------------------------------------------------------------------
CleanDates(list(patient = patient,
                intake = intake,
                laboratorium = laboratorium,
                lichond = lichond,
                leefrisico = leefrisico,
                behandelaar = behandelaar,
                medicatiereg = medicatiereg,
                medicatie = medicatie,
                oogonderzoek = oogonderzoek,
                voetonderzoek = voetonderzoek,
                aandoeningen = aandoeningen,
                aandoening = aandoening),
           120)

#--------------------------------------------------------------------------------------------------------------------------#
# Data opwerken ####
#--------------------------------------------------------------------------------------------------------------------------#
lichond1 <- lichond %>%
  mutate(lengtewaarde_clean = ifelse((lengtewaarde >0 & lengtewaarde <=300), as.numeric(lengtewaarde), as.numeric(NA)),
         gewichtwaarde_clean = ifelse((gewichtwaarde >0 & gewichtwaarde <=300), as.numeric(gewichtwaarde), as.numeric(NA)),
         bmi_clean = ifelse((bmi >=10 & bmi <=50), as.numeric(bmi), as.numeric(NA)),
         lengte_m = (lengtewaarde_clean/100),
         bmi_berekend = (gewichtwaarde/(lengte_m*lengte_m)),
         bmi_berekend_clean = ifelse((bmi_berekend >=10 & bmi_berekend <=50), as.numeric(bmi_berekend), as.numeric(NA)),
         bmi_bekend_clean_waarde = ifelse(!is.na(bmi_clean), as.numeric(bmi_clean),
                                          ifelse(!is.na(bmi_berekend_clean), as.numeric(bmi_berekend_clean), as.numeric(NA))),
         bmi_bekend_clean = ifelse(!is.na(bmi_bekend_clean_waarde), 1, NA),
         bd_sys_waarde = ifelse(sysbdhand >=30 & sysbdhand <=300 & !is.na(lichdat), as.numeric(sysbdhand), 
                                ifelse(sysbdthuis >=30 & sysbdthuis<=300 & !is.na(lichdat), as.numeric(sysbdthuis), 
                                       ifelse(sysbdautom >=30 & sysbdautom<=300 & !is.na(lichdat), as.numeric(sysbdautom), 
                                              ifelse(sysbd24uurs >=30 & sysbd24uurs<=300 & !is.na(lichdat), as.numeric(sysbd24uurs), as.numeric(NA))))),
         bd_sys = ifelse(!is.na(bd_sys_waarde), 1, NA),
         bd_dias_waarde = ifelse(diasbdhand >=15 & diasbdhand<=150 & !is.na(lichdat), as.numeric(diasbdhand), 
                                 ifelse(diasbdthuis >=15 & diasbdthuis<=150 & !is.na(lichdat), as.numeric(diasbdthuis), 
                                        ifelse(diasbdautom >=15 & diasbdautom<=150 & !is.na(lichdat), as.numeric(diasbdautom), 
                                               ifelse(diasbd24uurs >=15 & diasbd24uurs<=150 & !is.na(lichdat), as.numeric(diasbd24uurs), as.numeric(NA))))),
         bd_dias = ifelse(!is.na(bd_dias_waarde), 1, NA))

voetonderzoek1 <- voetonderzoek %>%
  mutate(voet_onderzoek =  ifelse(!is.na(voetdat), 1, NA)) 

oogonderzoek1 <- oogonderzoek %>%
  mutate(oog_onderzoek =  ifelse(!is.na(oogdat),1, NA)) 

laboratorium1 <- laboratorium %>%
  mutate(lab_hba1c_mol =  ifelse(!is.na(hba1cmol),1,0),
         lab_hba1c_perc = ifelse(!is.na(hba1cperc),1,0),
         hba1c_omgezet =  ((10.93 * hba1cperc) - 23.5),
         hba1cmol_bekend_clean = ifelse((hba1cmol >= 25 & hba1cmol <= 150 & !is.na(labdat)), as.numeric(hba1cmol), as.numeric(NA)),
         hba1cperc_omgezet_clean = ifelse((is.na(hba1cmol_bekend_clean) & hba1c_omgezet >= 25 & hba1c_omgezet <=150 & !is.na(labdat)), as.numeric(hba1c_omgezet), as.numeric(NA)),
         hba1c_bekend_clean = ifelse(!is.na(hba1cmol_bekend_clean), 1,
                                     ifelse(!is.na(hba1cperc_omgezet_clean), 1,0)),
         hba1c_bekend_waarde = ifelse(!is.na(hba1cmol_bekend_clean), as.integer(hba1cmol_bekend_clean),
                                      ifelse(!is.na(hba1cperc_omgezet_clean), as.integer(hba1cperc_omgezet_clean), as.integer(NA))),
         hba1c_verricht= ifelse(!is.na(hba1c_bekend_waarde), 1, NA),
         choltot_bekend_clean = ifelse((choltot >0 & choltot <= 30), as.numeric(choltot), as.numeric(NA)),
         hdl_clean = ifelse((cholhdl >0 & cholhdl <= 20), as.numeric(cholhdl), as.numeric(NA)),
         hdl_bekend_clean = ifelse(!is.na(hdl_clean), 1, NA),
         ldl_clean = ifelse((cholldl >0 & cholldl <= 20), as.numeric(cholldl), as.numeric(NA)),
         ldl_bekend_clean = ifelse(!is.na(ldl_clean), 1, NA),
         cholratio_bekend_clean = ifelse((cholratio >0 & cholratio <= 10), as.numeric(cholratio), as.numeric(NA)),
         triglyceriden__bekend_clean = ifelse((triglyceriden >0 & triglyceriden <= 30), as.numeric(triglyceriden), as.numeric(NA)),
         kreatinine_bekend_clean = ifelse((kreatinine > 0 & kreatinine <=1500), as.numeric(kreatinine), as.numeric(NA)),
         kreatinine_verricht = ifelse(!is.na(kreatinine_bekend_clean), 1, NA),
         kreatinineklaring_bekend_clean = ifelse((kreaklaring > 0 & kreaklaring <=500), as.numeric(kreaklaring), as.numeric(NA)),
         ckdepi_clean = ifelse((ckdepi > 0 & ckdepi <=100), as.numeric(ckdepi), as.numeric(NA)),
         cockcroft_clean = ifelse((cockcroft > 0 & cockcroft <=100), as.numeric(cockcroft), as.numeric(NA)),
         mdrd_clean = ifelse((mdrd > 0 & mdrd <=100), as.numeric(mdrd), as.numeric(NA)),
         egfr = ifelse(!is.na(ckdepi_clean), as.numeric(ckdepi_clean),
                       ifelse(!is.na(cockcroft_clean), as.numeric(cockcroftclean_clean), 
                              ifelse(!is.na(mdrd_clean), as.numeric(mdrd_clean), as.numeric(NA)))),
         egfr_bekend_clean = ifelse(!is.na(egfr), 1, NA),
         albukrea_bekend_clean = ifelse((albukrea >0 & albukrea <=100), as.numeric(albukrea), as.numeric(NA)),
         albumine_clean = ifelse((albumine > 0 & albumine <=500), as.numeric(albumine), as.numeric(NA)),
         albumine_bekend_clean = ifelse(!is.na(albumine_clean), 1, NA))

#--------------------------------------------------------------------------------------------------------------------------#
# Datacleaning en joinen van losse datasets tot een mergefile ####
#--------------------------------------------------------------------------------------------------------------------------#
intakesmetpturi <- intake %>% 
  select(patient_uri, uri, intake_id, intakedat) %>% 
  rename(intake_uri = uri)

intake2 <- intake %>% 
  select(patient_uri, uri, intake_id, intakedat, diagdat, diabetes_classification, insulinedat, polidat, ontslagpolidat) %>% 
  rename(intake_uri = uri) %>% 
  mutate_at(vars(matches("diagdat")), as.character) %>% 
  mutate_at(vars(matches("insulinedat")), as.character) %>% 
  mutate_at(vars(matches("polidat")), as.character) %>% 
  gather(var, val, -patient_uri, -intake_id, -intakedat, -intake_uri) %>%
  arrange(patient_uri) %>%
  distinct_all()

patient2 <- patient %>%
  rename(patient_uri = uri)  %>%
  left_join(intakesmetpturi, by = c("patient_uri" = "patient_uri")) %>% 
  mutate(datum = as.POSIXct(gebdat),
         typedatum = "gebdat") %>% 
  select(-organisation_uri, -is_valid, -gebdat) %>% 
  gather(var, val, -patient_uri, -datum, -typedatum, -intake_id, -intakedat, -intake_uri) %>%
  arrange(patient_uri) %>%
  distinct_all()

  # mutate(datum = as.POSIXct(gebdat),
  #        typedatum = "gebdat") %>% 
  
laboratorium2 <- laboratorium1 %>% 
  right_join(intakesmetpturi, by = c("intake_uri" = "intake_uri")) %>% 
  mutate(datum = labdat,
         typedatum = "labdat") %>% 
  select(-labdat) %>% 
  gather(var, val, -patient_uri, -datum, -typedatum, -intake_id, -intakedat) %>% 
  arrange(patient_uri, datum) %>% 
  distinct_all()

lichond2 <- lichond1 %>% 
  right_join(intake2, by = c("intake_uri" = "intake_uri")) %>% 
  mutate(datum = lichdat,
         typedatum = "lichdat") %>% 
  select(-lichdat) %>% 
  gather(var, val, -patient_uri, -datum, -typedatum, -intake_id, -intakedat) %>% 
  arrange(patient_uri, datum) %>% 
  distinct_all()

leefrisico2 <- leefrisico %>% 
  right_join(intakesmetpturi, by = c("intake_uri" = "intake_uri")) %>% 
  mutate(datum = leefdat,
         typedatum = "leefdat") %>% 
  select(-leefdat) %>% 
  gather(var, val, -patient_uri, -datum, -typedatum, -intake_id, -intakedat) %>% 
  arrange(patient_uri, datum) %>% 
  distinct_all()

behandelaar2 <- behandelaar %>% 
  right_join(intakesmetpturi, by = c("intake_uri" = "intake_uri")) %>% 
  mutate(datum = behdat,
         typedatum = "behdat") %>% 
  select(-behdat, -organisation_uri) %>% 
  gather(var, val, -patient_uri, -datum, -typedatum, -intake_id, -intakedat) %>% 
  arrange(patient_uri, datum) %>% 
  distinct_all()

medicatie2 <-  medicatie %>% 
  right_join(medicatiereg, by = c("medicatieregistratie_uri" = "uri")) %>% 
  right_join(intakesmetpturi, by = c("intake_uri" = "intake_uri")) %>% 
  mutate(datum = med_startdatum,
         typedatum = "med_startdatum") %>% 
  select(-med_startdatum, -meddat) %>% 
  mutate_at(vars(matches("med_einddatum")), as.character) %>% 
  gather(var, val, -patient_uri, -datum, -typedatum, -intake_id, -intakedat) %>% 
  arrange(patient_uri, datum) %>% 
  distinct_all()

oogonderzoek2 <- oogonderzoek1 %>%
  right_join(intakesmetpturi, by = c("intake_uri" = "intake_uri")) %>%
  mutate(datum = oogdat,
         typedatum = "oogdat") %>%
  select(-oogdat) %>%
  gather(var, val, -patient_uri, -datum, -typedatum, -intake_id, -intakedat) %>%
  arrange(patient_uri, datum) %>%
  distinct_all()

voetonderzoek2 <- voetonderzoek1 %>%
  right_join(intakesmetpturi, by = c("intake_uri" = "intake_uri")) %>%
  mutate(datum = voetdat,
         typedatum = "voetdat") %>%
  select(-voetdat) %>%
  gather(var, val, -patient_uri, -datum, -typedatum, -intake_id, -intakedat) %>%
  arrange(patient_uri, datum) %>%
  distinct_all()


df2 <- bind_rows(patient2, intake2, laboratorium2, lichond2, leefrisico2, behandelaar2, medicatie2, oogonderzoek2, voetonderzoek2)

df3 <- df2 %>% 
  drop_na(val) %>% 
  arrange(patient_uri, var, typedatum, desc(datum), val) %>% 
  distinct(patient_uri, var, .keep_all = T)

df3a <- df3 %>% 
  select(-intake_uri, -typedatum, -datum, -intakedat) %>% 
  pivot_wider(names_from = var,
              values_from = val)

df3b <- df3 %>% 
  select(patient_uri, intake_id, intakedat) %>% 
  mutate(datum = intakedat,
         typedatum = "intakedat") %>% 
  select(-intakedat)

df3c <- df3 %>% 
  select(patient_uri, intake_id, datum, typedatum)

df3d <- bind_rows(df3b, df3c) %>% 
  arrange(patient_uri, intake_id, typedatum, desc(datum)) %>% 
  distinct(patient_uri, intake_id, typedatum, .keep_all = T) %>% 
  drop_na(datum) %>% 
  pivot_wider(names_from = typedatum,
              values_from = datum)

df4 <- df3d %>% 
  left_join(df3a, by = c("patient_uri", "intake_id"))




# df3  <- df2 %>%
#   mutate(yearcorrect = ifelse(year(datum) == 2016 | 2017 | 2018 | 2019, 1, 
#                               ifelse(year(intakedat) == 2016 | 2017 | 2018 | 2019, 1, NA)))

##LEEFTIJD BEREKENEN

df2  <- df2 %>%
mutate(val_numeric = as.numeric(df2$val),
       datum = as.POSIXct(datum))

df2  <- df2 %>%
  filter(typedatum == "gebdat") %>%
  arrange(patient_uri, datum) %>%
  distinct(patient_uri, datum)

df2$leeftijd_vandaag <- CalculateAge(df2$datum, today(), 0, 130) 

summary(df2$leeftijd_vandaag)

length(unique(df2$patient_uri))

df2 <- bind_rows(patient2, intake2, laboratorium2, lichond2, leefrisico2, behandelaar2, medicatie2, oogonderzoek2, voetonderzoek2)

df2  <- df2 %>%
  mutate(val_numeric = as.numeric(df2$val),
         datum = as.POSIXct(datum))

## GESLACHT BEREKENEN 

df2  <- df2 %>%
  filter(var == 'geslacht') %>%
  # filter(val == 1) %>%
  arrange(patient_uri, var, val) %>%
  distinct(patient_uri, var, val)

table(df2$val)

#LEEFTIJD VAN DIAGNOSE

intake2 <- intake %>% 
  select(patient_uri, uri, intake_id, intakedat, diagdat, diabetes_classification, insulinedat, polidat, ontslagpolidat) %>% 
  rename(intake_uri = uri) %>% 
  mutate_at(vars(matches("diagdat")), as.character) %>% 
  mutate_at(vars(matches("insulinedat")), as.character) %>% 
  mutate_at(vars(matches("polidat")), as.character) %>% 
  gather(var, val, -patient_uri, -intake_id, -intakedat, -intake_uri, -diagdat) %>%
  arrange(patient_uri) %>%
  distinct_all()

patient2 <- patient %>%
  rename(patient_uri = uri)  %>%
  left_join(intakesmetpturi, by = c("patient_uri" = "patient_uri")) %>%
  left_join(intake2, by = c("patient_uri" = "patient_uri")) %>%
  select(patient_uri, gebdat, diagdat) %>%
  gather(var, val, -patient_uri, -diagdat) %>%
  arrange(patient_uri, diagdat, val) %>%
  distinct(patient_uri, diagdat, val)

patient2$leeftijd_diagnose <- CalculateAge(patient2$val, patient2$diagdat, 0, 130) 

summary(patient2$leeftijd_diagnose)

#diabetesduur


intake2 <- intake %>% 
  select(patient_uri, uri, intake_id, intakedat, diagdat, diabetes_classification, insulinedat, polidat, ontslagpolidat) %>% 
  rename(intake_uri = uri) %>% 
  mutate_at(vars(matches("diagdat")), as.character) %>% 
  mutate_at(vars(matches("insulinedat")), as.character) %>% 
  mutate_at(vars(matches("polidat")), as.character) %>% 
  gather(var, val, -patient_uri, -intake_id, -intakedat, -intake_uri, -diagdat) %>%
  arrange(patient_uri) %>%
  distinct_all()

patient2 <- patient %>%
  rename(patient_uri = uri)  %>%
  left_join(intakesmetpturi, by = c("patient_uri" = "patient_uri")) %>%
  left_join(intake2, by = c("patient_uri" = "patient_uri")) %>%
  select(patient_uri, gebdat, diagdat) %>%
  gather(var, val, -patient_uri, -diagdat) %>%
  arrange(patient_uri, diagdat) %>%
  distinct(patient_uri, diagdat)

patient2$duur_diabetes <- CalculateAge(patient2$diagdat, today(), 0, 130) 

summary(patient2$duur_diabetes)

#BMI, laatste 

df3 <- df2 %>% 
  filter(var == 'bmi_bekend_clean_waarde') %>%
  mutate(volledig = ifelse(!is.na(patient_uri) & !is.na(datum) & !is.na(val), 1, 0)) %>%
  filter(volledig == 1) %>%
  group_by(patient_uri) %>% 
  arrange(patient_uri, datum) %>% 
  arrange(desc(datum)) %>% 
  mutate(volgnummer = row_number()) %>% 
  mutate(last_bmi = ifelse((volgnummer == 1), as.numeric(val), as.numeric(NA))) %>%
  filter(!is.na(last_bmi))

summary(df3$last_bmi)

#categorien BMI

df3 <- df3 %>%
  mutate(bmi_cat = case_when(
    last_bmi < 20 ~ 1,
    last_bmi  >= 20 & last_bmi <25 ~ 2,
    last_bmi  >= 25 & last_bmi <30 ~ 3,
    last_bmi  >= 30 ~ 4,
    is.na(last_bmi ) ~ 5))

table(df3$bmi_cat)

#RR systolisch laatste 
df3 <- df2 %>% 
  filter(var == 'bd_sys_waarde') %>%
  mutate(volledig = ifelse(!is.na(patient_uri) & !is.na(datum) & !is.na(val), 1, 0)) %>%
  filter(volledig == 1) %>%
  group_by(patient_uri) %>% 
  arrange(patient_uri, datum) %>% 
  arrange(desc(datum)) %>% 
  mutate(volgnummer = row_number()) %>% 
  mutate(last_bdsys = ifelse((volgnummer == 1), as.numeric(val), as.numeric(NA))) %>%
  filter(!is.na(last_bdsys))

df3  <- df3 %>%
  mutate(val_numeric = as.numeric(val),
         datum = as.POSIXct(datum))

summary(df3$last_bdsys)

length(unique(df3$patient_uri))

df3 <- df3 %>%
  mutate(bmi_cat = case_when(
    last_bmi < 20 ~ 1,
    last_bmi  >= 20 & last_bmi <25 ~ 2,
    last_bmi  >= 25 & last_bmi <30 ~ 3,
    last_bmi  >= 30 ~ 4,
    is.na(last_bmi ) ~ 5))

table(df3$bmi_cat)

#RR diastolisch laatste 
df3 <- df2 %>% 
  filter(var == 'bd_dias_waarde') %>%
  mutate(volledig = ifelse(!is.na(patient_uri) & !is.na(datum) & !is.na(val), 1, 0)) %>%
  filter(volledig == 1) %>%
  group_by(patient_uri) %>% 
  arrange(patient_uri, datum) %>% 
  arrange(desc(datum)) %>% 
  mutate(volgnummer = row_number()) %>% 
  mutate(last_bddias = ifelse((volgnummer == 1), as.numeric(val), as.numeric(NA))) %>%
  filter(!is.na(last_bddias))

df3  <- df3 %>%
  mutate(val_numeric = as.numeric(val),
         datum = as.POSIXct(datum))

summary(df3$last_bddias)

length(unique(df3$patient_uri))

#LDL laatste 
df3 <- df2 %>% 
  filter(var == 'ldl_clean') %>%
  mutate(volledig = ifelse(!is.na(patient_uri) & !is.na(datum) & !is.na(val), 1, 0)) %>%
  filter(volledig == 1) %>%
  group_by(patient_uri) %>% 
  arrange(patient_uri, datum) %>% 
  arrange(desc(datum)) %>% 
  mutate(volgnummer = row_number()) %>% 
  mutate(last_ldl = ifelse((volgnummer == 1), as.numeric(val), as.numeric(NA))) %>%
  filter(!is.na(last_ldl))

df3  <- df3 %>%
  mutate(val_numeric = as.numeric(val),
         datum = as.POSIXct(datum))

summary(df3$last_ldl)

length(unique(df3$patient_uri))

#hDL laatste 
df3 <- df2 %>% 
  filter(var == 'hdl_clean') %>%
  mutate(volledig = ifelse(!is.na(patient_uri) & !is.na(datum) & !is.na(val), 1, 0)) %>%
  filter(volledig == 1) %>%
  group_by(patient_uri) %>% 
  arrange(patient_uri, datum) %>% 
  arrange(desc(datum)) %>% 
  mutate(volgnummer = row_number()) %>% 
  mutate(last_hdl = ifelse((volgnummer == 1), as.numeric(val), as.numeric(NA))) %>%
  filter(!is.na(last_hdl))

df3  <- df3 %>%
  mutate(val_numeric = as.numeric(val),
         datum = as.POSIXct(datum))

summary(df3$last_hdl)

length(unique(df3$patient_uri))

#HbA1c laatste 
df3 <- df2 %>% 
  filter(var == 'hba1c_bekend_waarde') %>%
  mutate(volledig = ifelse(!is.na(patient_uri) & !is.na(datum) & !is.na(val), 1, 0)) %>%
  filter(volledig == 1) %>%
  group_by(patient_uri) %>% 
  arrange(patient_uri, datum) %>% 
  arrange(desc(datum)) %>% 
  mutate(volgnummer = row_number()) %>% 
  mutate(last_hba1c= ifelse((volgnummer == 1), as.numeric(val), as.numeric(NA))) %>%
  filter(!is.na(last_hba1c))

df3  <- df3 %>%
  mutate(val_numeric = as.numeric(val),
         datum = as.POSIXct(datum))

summary(df3$last_hba1c)

length(unique(df3$patient_uri))

#Diabetes classification 
df3 <- df2 %>% 
  filter(var == 'diabetes_classification') %>%
  mutate(volledig = ifelse(!is.na(patient_uri) & !is.na(val), 1, 0)) %>%
  filter(volledig == 1) %>%
  group_by(patient_uri) %>% 
  arrange(patient_uri, intakedat) %>% 
  arrange(desc(intakedat)) %>% 
  mutate(volgnummer = row_number()) %>% 
  mutate(last_class = ifelse((volgnummer == 1), as.numeric(val), as.numeric(NA))) %>%
  filter(!is.na(last_class))

df3  <- df3 %>%
  mutate(val_numeric = as.numeric(val),
         datum = as.POSIXct(datum))


table(df3$last_class)

length(unique(df3$patient_uri))



# df3  <- df2 %>%
#   filter(var == 'mdrd' & typedatum == 'labdat') %>%
#   filter(year(datum) == 2018 & 2019)


       
# Year(datok) %in% c(2018:2020)

 
 # df3  <- df2 %>%
 #   filter(var == 'mdrd' & typedatum == 'labdat') %>%
 #   filter(year(datum) == 2018 & 2019)



# df3  <- df2 %>%
#   filter(typedatum == "gebdat") %>%
#   mutate(as.POSIXct(datum)) %>%
#   mutate(as.POSIXct(intakedat)) %>%
#   arrange(patient_uri) %>%
#   distinct(patient_uri, .keep_all= TRUE)

# 
# df3  <- df3 %>%
#   filter(typedatum == "labdat") %>%
#   mutate(datum = as.POSIXct(datum)) %>%
#   mutate(hba1cdatum = ifelse((var == "hba1c_verricht" & val == 1), datum, NA))

 
 
 # df4  <- df3 %>%
 #     filter(var == 'bmi_bekend_clean_waarde') %>%
 #   mutate(bmi_cat = case_when(
 #   val < 18.5 ~ 1,
 #   val >= 18.5 & val <25 ~ 2,
 #   val >= 25 & val <30 ~ 3,
 #   val >= 30 ~ 4,
 #   is.na(val) ~ 5))
 # 
 # table(df4$bmi_cat)
 # 
 
#
# summary(df3$val_numeric)
# 
# df4  <- df3 %>%
# filter(is.na(val_numeric))
# 
# df4  <- df3 %>%
#   filter(!is.na(val_numeric))
#--------------------------------------------------------------------------------------------------------------------------#
#  ####
#--------------------------------------------------------------------------------------------------------------------------#


