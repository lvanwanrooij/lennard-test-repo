library(tidyverse)

df <- readxl::read_excel("S:/Informatie/DST/Rapportages/TP data/Openbaar databestand MSZ verslagjaar 2019 - 20200706.xlsx")
table(df$Bron, useNA = "always") 
dica <- df %>% 
  filter(!Bron %in% c("LROI", "LTR", "NHR", "NULL", "Renine"))

library(plotly)
library(ggplotly)

df <- dica %>% 
  count(OrganisatieNaam, Bron) %>% 
  group_by(OrganisatieNaam) %>% 
  mutate(total = sum(n)) %>% 
  ungroup

plot_ly(df, x = ~OrganisatieNaam, y = ~n, color = ~Bron, type= 'bar') %>% 
  layout(barmode = 'stack')


tbo_files <- list.files("H:/My Documents/dica-indicator-syntax/syntax/kpi-2020/", pattern = "textbook", recursive = T, full.names = T)
myf <- function(file) {
  setwd(dirname(file))
  tc <- readLines(file)
  tc <- tc[1:grep("##### (1) CREATE INEXCLUSIONLIST, (2) AGGREGRATE ON PROVIDER AND REGION LEVEL AND (3) SAVE OUTPUT AS RDS -----", tc, fixed = T)]
  source(textConnection(tc))
  df
}

myf <- Vectorize(myf)
tbo_outs <- myf(tbo_files)
names(tbo_outs) <- gsub("\\.R", "", basename(tbo_files))
tbo_all <- bind_rows(tbo_outs, .id = "ind")

data_for_plot <- tbo_all %>% 
  drop_na(jaarrap) %>% 
  group_by(ind, jaarrap) %>% 
  summarise(perc = round(100*sum(teller, na.rm = T)/sum(noemer, na.rm = T)), 1) %>% 
  arrange(ind, jaarrap)


plot_ly(data_for_plot, x = ~jaarrap, y = ~perc, color = ~ind, mode = 'lines') %>% layout(legend = list(orientation = 'h'))

