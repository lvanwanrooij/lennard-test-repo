#load libraries, when necessary after installing ####
if (!require(editData)) install.packages('editData')
library(editData)
if (!require(miniUI)) install.packages('miniUI')
library(miniUI)
if (!require(shiny)) install.packages('shiny')
library(shiny)

#create myapp function####
myapp <- function(data = NULL){
  context <- rstudioapi::getActiveDocumentContext()
  text <- context$selection[[1]]$text
  mydata = deparse(substitute(data))

  ui <- miniPage(gadgetTitleBar("Welkom in de app!"),
                 miniContentPanel(fluidRow(
                   column(12, fileInput("file1", "Upload RDS file")),
                   column(10, "Om de rekenregels op de juiste manier aan te vullen zijn er een aantal regels:"),
                   column(2, textInput3("mydata", label = "registratie", value = mydata, width = 100, bg = "lightcyan")),
                   column(12, "-	De rekenregels vul je in in de kolom “rekenregel noemer” en “rekenregel teller”. Niet op de plek van conditie. Deze moet leeg zijn"),
                   column(12, "-	In de rekenregel komt de variabele met daarachter de optieset."),
                   column(12, "-	Analyseerbaar staat niet in de rekenregel."),
                   column(12, "-	Variabelennamen met kleine letter"),
                   column(12, "-	Gebruik en/ of en geen tekens tussen variabelen"),
                   column(8, "-	Gebruik < of > in plaats van t/m"),
                   column(4, "Voorbeelden:"),
                   column(8, "-	Gebruik een spatie voor en na een teken"),
                   column(4, "tumbulk = 1 EN tumkeus = 1 EN aardok = 1."),
                   column(8, "-	Gebruik ',' of 't/m' wanneer je de verschillende antwoordopties weer wilt geven. Dus: status = 0, 1. NB: een extra spatie achter de komma!"),
                   column(4, "Mediane tijd in dagen tussen datok EN datovl"),
                   column(12, "-")),
    editableDTUI("table1"),
    downloadButton("downloadData", 
                   "Download as RDS")))

  server = function(input, output, session) {
    mydf = reactive({
      mydf = eval(parse(text = input$mydata))
      mydf
    })

    df = callModule(editableDT, "table1", data = reactive(mydf()),
                    inputwidth = reactive(500))
    
    observeEvent(input$file1, {
      if (!is.null(input$file1)) {
        uploaded <<- readRDS(input$file1$datapath)
        updateTextInput(session, "mydata", value = "uploaded")
      }
    })

    output$downloadData <- downloadHandler(filename = function() {
      paste("edited-", Sys.time(), ".rds")
    }, content = function(file) {
      saveRDS(df(), file)
    })
    
    observeEvent(input$done, {
      result = df()
      #saveRDS(result, paste0("S:/Registraties/Indicatoren knaldagen/Rekenregels/2. InProgress/", result[1,1],"_InProgress.rds"))
      stopApp(result)
    })
    observeEvent(input$cancel, {
      stopApp()
    })
  }

  myviewer <- browserViewer()
  runGadget(ui, server, viewer = myviewer)
}



