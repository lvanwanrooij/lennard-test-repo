#!/bin/env Rscript
modifiedfiles <- list.files("H:/My Documents/dica-indicator-syntax/", recursive = T)
modifiedfiles <- modifiedfiles[grepl("syntax/kpi", modifiedfiles)]

modifiedfiles <- modifiedfiles[!grepl("clinical-update-status|README|codman|dica-pack|Rproj|proms|dbir|dasa-deur-tot-deur-tot-lies-tijd", modifiedfiles)]
modifiedfiles <- modifiedfiles[!nchar(modifiedfiles) %in% c(20, 22, 24)] 


wd <- paste0("H:/My Documents/dica-indicator-syntax/syntax/kpi/",gsub("syntax/kpi/(.+)/.*.", "\\1", modifiedfiles))
modifiedfiles <- paste0("H:/My Documents/dica-indicator-syntax/", modifiedfiles)
invisible(suppressWarnings(do.call(file.remove, list(list.files("H:/My Documents/dica-indicator-syntax/output/kpi", full.names = TRUE)))))

## gecommente scripts apart houden, zodat ze niet gesourced worden
commented <- integer()
for(i in 1:length(modifiedfiles)){
  
  ind_code <- readLines(modifiedfiles[i])
  if(all(grepl("#", ind_code) | ind_code == "")){
    commented <- c(commented, i)
  } 
  
}

commented <- modifiedfiles[commented]
if(length(modifiedfiles) > 0){
  commented <- data.frame(script = commented, status = "commented")
}
modifiedfiles <- modifiedfiles[!(modifiedfiles %in% commented$script)]





# check1:  check op errors in scripts
a <- 0
error_when_sourcing <- c()
for(i in 1 : length(modifiedfiles)){
  a <- a + 1
  
  setwd(sub("/[^/]+$", "", modifiedfiles[i]))
  error <- try(source(modifiedfiles[i]))[1]
  if (error == "NULL"){
    error_when_sourcing[a] <- paste(gsub("H:/My Documents/dica-indicator-syntax/syntax/kpi/", "", modifiedfiles)[i], "Script ran successfully", sep = "@___@")
  } else {
    error_when_sourcing[a] <- paste(gsub("H:/My Documents/dica-indicator-syntax/syntax/kpi/", "", modifiedfiles)[i], error, sep = "@___@")
  }
}

sourcing_errorlog <- data.frame(script = gsub("H:/My Documents/dica-indicator-syntax/syntax/kpi/", "", modifiedfiles),
                                error = sapply(sapply(error_when_sourcing, strsplit, "@___@"), "[", 2),
                                row.names = NULL, stringsAsFactors = F)

# einde check 1

# begin check 2
outputfilenames <- list.files("H:/My Documents/dica-indicator-syntax/output/kpi", full.names = T)[!list.files("H:/My Documents/dica-indicator-syntax/output/kpi") == "log"]
element_indicator_metadata <- outputfilenames[grep(".element-indicator-metadata.rds", outputfilenames)]

correctmeta <- c("version",	"registry",	"year",	"uri",	"type",	"visual",	"order",	"isidCode",	"inidCode",	"shortLabel",	"longLabel",
                 "chapter",	"isExternal",	"startMonth",	"monthCount",	"isCasemix",	"denominatorLabel",	"denominatorCondition",
                 "numeratorLabel",	"numeratorCondition",	"explanation1",	"explanation2",	"explanation3",
                 "isTransparent",	"casemixFactors",	"casemixVariables",	"excluded",	"isDICA", "isIGJ",
                 "denominatorDefinition",	"numeratorDefinition")

list_metadata <- lapply(element_indicator_metadata, readRDS)

metadata_errorlog <- data.frame(script = gsub("H:/My Documents/dica-indicator-syntax/output/kpi/|.element-indicator-metadata.rds", "", element_indicator_metadata),
                                metakolomcheck = sapply(list_metadata, function(i) sum(names(i) %in% correctmeta) == length(correctmeta) & length(i) == length(correctmeta)),
                                typecheck = sapply(list_metadata, "[[", "type") %in% paste0(c("average", "boolean", "count", "funnel", "median", "percentage"), "-indicator"),
                                visual_check =  sapply(list_metadata, "[[", "visual") %in% c("provider-region-percentage-funnel-and-table", "provider-count-table",
                                                                                             "provider-region-median-table", "provider-boolean-table", "provider-region-percentage-table"),
                                order_check = nchar(sapply(list_metadata, "[[", "order")) > 0,
                                shortLabel_check = nchar(sapply(list_metadata, "[[", "shortLabel")) > 0,
                                longLabel_check = nchar(sapply(list_metadata, "[[", "longLabel")) > 0,
                                chapter_check = sapply(list_metadata, "[[", "chapter") %in% paste0(c("Proces", "Structuur", "Uitkomst"), "indicatoren"),
                                startMonth_check = sapply(list_metadata, "[[", "startMonth") %in% c(1:12),
                                monthCount_check = sapply(list_metadata, "[[", "monthCount") %in% c(9, 12, 24, 36),
                                isTransparent_check =  sapply(list_metadata, "[[", "isTransparent") %in%  c(0, 1, 2),
                                isDICA_check =  sapply(list_metadata, "[[", "isDICA") %in% c(0, 1),
                                isIGJ_check =  sapply(list_metadata, "[[", "isIGJ") %in% c(0, 1)
)
#einde check 2

#begin check 3
uri_vis_out <- do.call("bind_rows", lapply(list_metadata, function(x) x[c("order", "uri", "visual")]))

outputs_per_uri <- data.frame(uri = gsub("H:/My Documents/dica-indicator-syntax/output/kpi/", "", sapply(sapply(outputfilenames, strsplit, "\\."), "[", 1)),
                              outputs = sapply(sapply(outputfilenames, strsplit, "\\."), "[", 2),
                              row.names = NULL, stringsAsFactors = F)

uri_vis_out %<>%
  left_join(outputs_per_uri, by = "uri") %>%
  arrange(order, uri, outputs) %>%
  group_by(uri, visual) %>%
  summarise(outputs = paste(outputs, collapse =  "^")) %>%
  ungroup()

correct_vis_out <- data.frame(visual =
                                c(rep("provider-count-table", 3),
                                  rep("region-count-table", 3),
                                  rep("provider-boolean-table", 2),
                                  rep("region-boolean-table", 2),
                                  rep("provider-region-percentage-funnel-and-table", 5),
                                  rep("provider-region-corrected-percentage-funnel-and-table", 5),
                                  rep("provider-region-percentage-table", 4),
                                  rep("provider-region-corrected-percentage-table", 4),
                                  rep("provider-region-avg-table", 4),
                                  rep("provider-region-median-table", 4)),
                              
                              outputs =
                                c(
                                  "element-indicator-metadata",	"provider-count-indicator", "patient-indicator-basic-inclusion-list",
                                  "element-indicator-metadata",	"region-count-indicator", "patient-indicator-basic-inclusion-list",
                                  "element-indicator-metadata",	"provider-boolean-indicator",
                                  "element-indicator-metadata",	"region-boolean-indicator",
                                  "element-indicator-metadata",	"provider-percentage-indicator",	"region-percentage-indicator",	"region-funnel-confidence-interval", "patient-indicator-basic-inclusion-list",
                                  "element-indicator-metadata",	"provider-corrected-percentage-indicator",	"region-percentage-indicator",	"region-corrected-funnel-confidence-interval", "patient-indicator-basic-inclusion-list",
                                  "element-indicator-metadata",	"provider-percentage-indicator",	"region-percentage-indicator", "patient-indicator-basic-inclusion-list",
                                  "element-indicator-metadata",	"provider-corrected-percentage-indicator",	"region-percentage-indicator", "patient-indicator-basic-inclusion-list",
                                  "element-indicator-metadata",	"provider-average-indicator",	"region-average-indicator", "patient-indicator-basic-inclusion-list",
                                  "element-indicator-metadata",	"provider-median-indicator",	"region-median-indicator", "patient-indicator-basic-inclusion-list"),
                              
                              stringsAsFactors = F)

correct_vis_out %<>%
  arrange(visual, outputs) %>%
  group_by(visual) %>%
  summarise(outputs = paste(outputs, collapse =  "^"))

check_on_output_types <- uri_vis_out %>%
  left_join(correct_vis_out, by = "visual", suffix = c("_current", "_correct")) %>%
  mutate(all_output_types_correct = outputs_current == outputs_correct) %>%
  select(uri, all_output_types_correct)
#einde check 3

#maak checklist
checklist <- list(sourcing_errorlog = sourcing_errorlog,
                  metadata_errorlog = metadata_errorlog,
                  check_on_output_types = check_on_output_types)

if(sum(sourcing_errorlog$error == "Script ran successfully") != nrow(sourcing_errorlog) |
   sum(metadata_errorlog == TRUE) != dim(metadata_errorlog)[1] * (dim(metadata_errorlog)[2] - 1) |
   sum(check_on_output_types$all_output_types_correct) != nrow(check_on_output_types)){
  writexl::write_xlsx(checklist, "../../../output/checklist.xlsx")
} else {
  print("Everything OK: commit succeeded, you can push now!")
}
