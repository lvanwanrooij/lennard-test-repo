---
author: "Dutch Institute for Clinical Auditing"
date: "`r format(Sys.time(), '%d %B, %Y -- %H:%M')`"
title: "`r paste('Teller Noemer check', params$reg_to_check)`"
output: 
  html_document:
    css: H:/My Documents/dica-test-repository/syntax/apps/outputcheck/dica_style.css
params:
  reg_to_check:
    label: "registratie"
    value: daci
    input: select
    choices: [daci, dasa, dato, dbir, dcra, dgeadrce, dgoa, dhba, dhfa, dhna, dlcal, dlcar, dlcas, dmtr, dpard, dpca, dsaa, dssr, duca, epsa, nbca]
---
  
  
```{r, echo=FALSE, include=FALSE, warning=FALSE, message=FALSE}

reg_to_check = params$reg_to_check
#reg_to_check = "dssr"

devtools::load_all("H:/My Documents/dica-indicator-syntax/packages/dicapackage")

noemer_data = readRDS("H:/My Documents/dica-test-repository/syntax/indicatoren-check/teller-noemer-check/noemer_data.rds")
teller_data = readRDS("H:/My Documents/dica-test-repository/syntax/indicatoren-check/teller-noemer-check/teller_data.rds")
uri_order = readRDS("H:/My Documents/dica-test-repository/syntax/indicatoren-check/teller-noemer-check/uri_order.rds")

# test case met epsa noemers, hiervan is data al gestructureerd

noemer_data = noemer_data %>% filter(registry == reg_to_check)
teller_data = teller_data %>% filter(registry == reg_to_check)
indicator_data = bind_rows(noemer_data, teller_data)

uri_order = uri_order %>% filter(registry == reg_to_check)

# source and read in indicator output
#files = paste0("H:/My Documents/dica-indicator-syntax/syntax/kpi-2020/", reg_to_check, "/", required_indicators, ".R")

#invisible(suppressWarnings(do.call(file.remove, list(list.files("H:/My Documents/dica-indicator-syntax/output/kpi-2020", full.names = TRUE)))))
current_wd <- getwd()

get_outputtypes_from_indicators <- function(registry){
  setwd(paste0("S:/Registraties/ALGEMEEN/GIT repository gedeeld met MRDM/3. staging/syntax/kpi-2020/", registry))
  scripts <- list.files(paste0("S:/Registraties/ALGEMEEN/GIT repository gedeeld met MRDM/3. staging/syntax/kpi-2020/", registry), recursive = T, full.names = T)
  required_indicators = uri_order$uri[uri_order$order %in% na.omit(unique(c(indicator_data$order, indicator_data$noemerref_order, indicator_data$tellerref_order)))]
  relevant_indicatorscripts <- scripts[grepl(paste(required_indicators, collapse = "|"), scripts)]
  
  run_most_of_indicatorscript <- function(indicatorscript) {
    
    indicatorscript <- scan(indicatorscript, what=character(), sep="\n", quiet=TRUE, encoding = "UTF-8")
    indicatorscript <- indicatorscript[!grepl("^saveRDS|^source", indicatorscript)]
    indicatorscript <- prepend(indicatorscript, "metadata <- NULL; inexclusielijst <- NULL; providerdf <- NULL; regiondf <- NULL; cidf <- NULL;")
    tc <- textConnection(indicatorscript)
    source(tc)
    close(tc)
    
    outputtypes_current_script <- list(metadata = metadata,
                                       inexclusielijst = inexclusielijst,
                                       providerdf = providerdf,
                                       regiondf = regiondf,
                                       cidf = cidf)
    
    outputtypes_current_script
  }
  
  outputs <- map(relevant_indicatorscripts, run_most_of_indicatorscript)
  names(outputs) <- map(outputs, "metadata") %>% map_chr("uri")
  outputs
}

reg_outputs <- get_outputtypes_from_indicators(reg_to_check)
reg_outputs_providerdf <- reg_outputs %>% map("providerdf")


# sourcefile <- function(file) {
#   source(file, chdir = T)
# }

# safesource <- safely(sourcefile)
# sourceinfo <- map(files, safesource)

# outputfiles = list.files("H:/My Documents/dica-indicator-syntax/output/kpi-2020", pattern = ".rds", full.names = TRUE)
# outputfiles = outputfiles[grepl("provider-percentage-indicator|provider-corrected-percentage-indicator|provider-count-indicator|provider-median-indicator", outputfiles)]
    
# output = map(outputfiles, function(x){
#   readRDS(x) %>%
#     mutate(uri = gsub(".*/", "", gsub("\\..*", "", x)))
# })

output = bind_rows(reg_outputs_providerdf, .id = "uri")

numerator_cols = c("numerator", "count")[c("numerator", "count") %in% colnames(output)]
denominator_cols = c("denominator", "frequency")[c("denominator", "frequency") %in% colnames(output)]

output = output %>%
  mutate(output_teller = coalesce(!!!syms(numerator_cols)),
         output_noemer = coalesce(!!!syms(denominator_cols)))

output = output %>%
  left_join(uri_order, by = "uri")

output_indicator_data = output %>%
  full_join(indicator_data, by = "order")

## Noemer check
output_noemer_data = output_indicator_data %>%
  left_join(output_indicator_data %>% select(output_noemer, output_teller, order, year, provider), by = c("noemerref_order" = "order", "year", "provider"), suffix = c("", "_ref")) %>%
  mutate(ref = case_when(grepl("noemer", noemer) ~ as.integer(output_noemer_ref),
                         grepl("teller", noemer) ~ as.integer(output_teller_ref),
                         TRUE ~ as.integer(NA))) %>%
  select(provider, year, order, reference = noemer, output_noemer, reference_output = ref) %>%
  filter(!is.na(reference_output))

noemer_check = output_noemer_data %>%
  mutate(check = as.character(output_noemer == reference_output)) %>%
  filter(!is.na(check))

# Teller check
output_teller_data = output_indicator_data %>%
  left_join(output_indicator_data %>% select(output_noemer, output_teller, order, year, provider), by = c("tellerref_order" = "order", "year", "provider"), suffix = c("", "_ref")) %>%
  mutate(ref = case_when(grepl("noemer", teller) ~ as.integer(output_noemer_ref),
                         grepl("teller", teller) ~ as.integer(output_teller_ref),
                         TRUE ~ as.integer(NA))) %>%
  select(provider, year, order, reference = teller, output_teller, reference_output = ref) %>%
  filter(!is.na(reference_output))

teller_check = output_teller_data %>%
  mutate(check = as.character(output_teller == reference_output)) %>%
  filter(!is.na(check))

## Overview

overview = bind_rows(noemer_check, teller_check) %>%
  mutate(mismatch = case_when(!is.na(output_noemer) ~ abs(output_noemer - reference_output),
                                     !is.na(output_teller) ~ abs(output_teller - reference_output)))

```

```{r, include=FALSE}
library(DT)
datatable(NULL)
```


##  {.tabset}

### Overview

```{r, echo=FALSE, message=FALSE, warning=FALSE}

summ = overview %>%
  group_by(order) %>%
  summarise(no_mismatch = all(mismatch == 0)) %>%
  ungroup() %>%
  arrange(no_mismatch)

datatable(summ, 
          class = "display nowrap",
          options = list(scrollX = TRUE)) %>%
  formatStyle(names(summ),
              backgroundColor = styleEqual(c("true", "false"), c("#d4e3db", "#d6cf40")))


# summ_per_ind = overview %>%
#   group_by(order) %>%
#   summarise(total_mismatch = sum(mismatch)) %>%
#   ungroup()
# 
# plotly::plot_ly(y = summ_per_ind$total_mismatch, 
#                           x = summ_per_ind$order,
#                           type = "bar",
#                         marker = list(color = "#d4e3db"))

```

### Noemer

```{r, echo=FALSE, warning=FALSE}

datatable(noemer_check, 
          class = "display nowrap",
          options = list(scrollX = TRUE)) %>%
  formatStyle(names(noemer_check),
              backgroundColor = styleEqual(c("TRUE", "FALSE"), c("#d4e3db", "#d6cf40")))

```

### Teller

```{r echo=FALSE, warning=FALSE}

datatable(teller_check, 
          class = "display nowrap",
          options = list(scrollX = TRUE)) %>%
  formatStyle(names(teller_check),
              backgroundColor = styleEqual(c("TRUE", "FALSE"), c("#d4e3db", "#d6cf40")))
```