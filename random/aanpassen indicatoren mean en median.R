x <- read.table(text = "
           dhfa-doorlooptijden-operatie.R
dlcas-gecompliceerd-beloop-na-electieve-resectie-casemix.R
dgoa-mediaan-aantal-dagen-vulvacarcinoom.R
dgoa-mediaan-aantal-dagen-ovariumcarcinoom-neoadjuvant.R
dgoa-mediaan-aantal-dagen-ovariumcarcinoom.R
dgoa-mediaan-aantal-dagen-ovariumcarcinoom-laag-stadium.R
dgoa-mediaan-aantal-dagen-cervixcarcinoom.R
dgoa-mediaan-aantal-dagen-ovariumcarcinoom-behandeling-tot-chemotherapie.R
nbca-doorlooptijd-biopt-eerste-operatie.R
nbca-doorlooptijd-laatste-operatie-adjuvante-chemotherapie.R
nbca-doorlooptijd-laatste-operatie-adjuvante-totaal.R
nbca-doorlooptijd-biopt-directe-reconstructie.R
nbca-doorlooptijd-diagnose-start-behandeling.R
nbca-doorlooptijd-biopt-neoadjuvante-chemo.R
nbca-doorlooptijd-laatste-operatie-adjuvante-radiotherapie.R
dhba-opnameduur-bij-patienten-die-een-percutane-leverablatie-ondergaan.R
dhba-opnameduur-bij-patienten-die-een-major-leverresectie-ondergaan.R
dhba-opnameduur-bij-patienten-die-een-minor-leverresectie-ondergaan.R
dhba-opnameduur-bij-patienten-die-een-open-of-laparoscopische-ablatie-ondergaan.R
dlcar-90-dagen-mortaliteit-chemoradiotherapie-sclc-concomitant-meer23.R
dhna-mediane-doorlooptijd-poli-laatste-mdo.R
dhna-mediane-doorlooptijd-poli-start-behandeling.R
dhna-mediane-doorlooptijd-verwijzing-poli.R
epsa-gastroschizis-duration-until-full-enteral-feeding.R
epsa-esophageal-atresia-anastomotic-stricture-mean.R
epsa-esophageal-atresia-anastomotic-stricture-median.R
dpca-mediane-wachttijd.R
dato-respons-proms.R
dato-indicatiestelling.R
dmtr-type-behandeling-antipd1-antilichaam-adjuvant.R
dmtr-proms-bij-tweede-meetmoment.R
dasa-deur-tot-deur-tot-lies-tijd.R
dpard-oogonderzoek-verricht-onder-behandeling-internist.R
dpard-hba1c-hoog-jonger-dan-18-onder-behandeling-kinderarts.R
dpard-egfr-lager-dan-60-onder-behandeling-internist-bij.R
dpard-bmi-bekend-onder-behandeling-internist.R
dpard-diastolische-bloeddruk-gemeten.R
dpard-hba1c-laag-jonger-dan-18-onder-behandeling-kinderarts.R
dpard-hba1c-laag-ouder-dan-18-onder-behandeling-kinderarts.R
dpard-leefstijl-en-risicofactoren-bekend-onder-behandeling-internist.R
dpard-hba1c-laag-onder-behandeling-internist.R
dpard-hba1c-hoog-onder-behandeling-internist.R
dpard-systolische-bloeddruk-gemeten.R
dpard-voetzorg-volwassenen.R
dpard-hba1c-hoog-ouder-dan-18-onder-behandeling-kinderarts.R")

y <- read.table(text = "
                dlcas-gecompliceerd-beloop-na-electieve-resectie-casemix.R
dlcar-90-dagen-mortaliteit-chemoradiotherapie-sclc-concomitant-meer23.R
dato-respons-proms.R
dato-indicatiestelling.R
dmtr-type-behandeling-antipd1-antilichaam-adjuvant.R
dmtr-proms-bij-tweede-meetmoment.R
dpard-oogonderzoek-verricht-onder-behandeling-internist.R
dpard-hba1c-hoog-jonger-dan-18-onder-behandeling-kinderarts.R
dpard-egfr-lager-dan-60-onder-behandeling-internist-bij.R
dpard-bmi-bekend-onder-behandeling-internist.R
dpard-diastolische-bloeddruk-gemeten.R
dpard-hba1c-laag-jonger-dan-18-onder-behandeling-kinderarts.R
dpard-hba1c-laag-ouder-dan-18-onder-behandeling-kinderarts.R
dpard-leefstijl-en-risicofactoren-bekend-onder-behandeling-internist.R
dpard-hba1c-laag-onder-behandeling-internist.R
dpard-hba1c-hoog-onder-behandeling-internist.R
dpard-systolische-bloeddruk-gemeten.R
dpard-hba1c-hoog-ouder-dan-18-onder-behandeling-kinderarts.R
dpard-voetzorg-volwassenen.R
")

z <- setdiff(x,y)

z$V2 <- sapply(sapply(z$V1, function(x) strsplit(x, split = "-")), "[", 1)
colnames(z)
metagui::mygsubfilefunc(z$fullpath,
                        "(.*)", '\\1\n', F)

z$fullpath <- paste0("H:/My Documents/dica-indicator-syntax/syntax/kpi-2020/", z$V2, "/", z$V1)
z %>% 
  select(V1)
